// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using UnityEngine;

namespace Virtuloc.Utils
{
    public static class MathUtils
    {
        /// <summary>
        /// Map value from old rande to new range.
        /// Input value will be clamped to old range.
        /// </summary>
        /// <param name="oldMin"></param>
        /// <param name="oldMax"></param>
        /// <param name="newMin"></param>
        /// <param name="newMax"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static float MapValue(float oldMin, float oldMax, float newMin, float newMax, float value)
        {
            value = Mathf.Clamp(value, oldMin, oldMax);

            var oldRange = oldMax - oldMin;
            var newRange = newMax - newMin;
            var newValue = ((value - oldMin) * newRange / oldRange) + newMin;

            return newValue;
        }
    }
}
