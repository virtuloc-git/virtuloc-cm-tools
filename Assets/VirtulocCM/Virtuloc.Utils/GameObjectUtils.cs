using UnityEngine;

public static class GameObjectUtils
{
    public static T GetComponentByInterfaceInParentRecursively<T>(this Component start) where T : class
    {
        T component = start.GetComponentInParent<T>();

        if (component != null)
        {
            return component;
        }

        if (start.transform.parent == null)
        {
            return null;
        }

        return GetComponentByInterfaceInParentRecursively<T>(start.transform.parent);
    }

    public static T GetComponentByInterfaceInChildrenRecursively<T>(this Transform start) where T : class
    {
        T component = start.GetComponentInChildren<T>();

        if (component != null)
        {
            return component;
        }

        if (start.transform.parent == null)
        {
            return null;
        }

        foreach (Transform child in start.transform)
        {
            var comp = GetComponentByInterfaceInChildrenRecursively<T>(child);

            if (comp != null)
                return comp;
        }

        return null;
    }

        public static Transform GetHighestParent(this Transform start)
    {
        if (start.parent == null)
            return start;
        else
            return GetHighestParent(start.parent);
    }

}
