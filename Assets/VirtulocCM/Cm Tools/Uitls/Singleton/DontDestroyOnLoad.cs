﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.Utils
{
	public class DontDestroyOnLoad : MonoBehaviour
	{

		// Use this for initialization
		void Start()
		{
			DontDestroyOnLoad(gameObject);
		}

	}
}
