﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.Utils
{

    public class Singleton<T> : MonoBehaviour where T : Component
    {

        public static bool SINGLETON_DEBUG;

        private bool eventRegistered;
        private static bool isQuit = false;
        protected static T instance;

        public bool DontDestroy
        {
            get
            {
                return gameObject.GetComponent<DontDestroyOnLoad>() != null;
            }
            set
            {
                if (!value && DontDestroy)
                    GameObject.Destroy(GetComponent<DontDestroyOnLoad>());

                if (value && !DontDestroy)
                    gameObject.AddComponent<DontDestroyOnLoad>();
            }
        }

        /**
           Returns the instance of this singleton.
        */
        public static T Instance
        {
            get
            {
                if (instance == null && !isQuit)
                {
                    instance = (T)FindObjectOfType(typeof(T));

                    if (instance == null)
                    {
                        GameObject go = new GameObject(typeof(T).Name);
                        instance = go.AddComponent<T>();
                    }
                }

                return instance;
            }
        }

        public static bool IsCreated
        {
            get { return instance != null; }
        }

        public static bool IsInstanceNull()
        {
            return instance == null;
        }

        public void ClearInstances()
        {
            UnityEngine.Object[] instances = FindObjectsOfType(typeof(T));

            if (instances.Length > 1 && instance != this)
            {
                Destroy(gameObject);
            }
        }

        public virtual void Awake()
        {
            if (!IsCreated)
                instance = Instance;
            else
            {
                if (instance != this)
                    Destroy(gameObject);
            }

            if (SINGLETON_DEBUG)
                Debug.Log(String.Format("{0}.Awake()", typeof(T).Name));

            if (!eventRegistered)
                RegisterEvents();
        }

        public virtual void OnApplicationQuit()
        {
            isQuit = true;
        }

        public virtual void OnDestroy()
        {
            if (!isQuit)
            {
                if (eventRegistered)
                    UnRegisterEvents();
            }

            instance = null;
        }

        protected virtual void RegisterEvents()
        {
            if (SINGLETON_DEBUG)
                Debug.Log(String.Format("{0}.RegisterEvents()", typeof(T).Name));

            eventRegistered = true;
        }

        protected virtual void UnRegisterEvents()
        {

            eventRegistered = false;
        }
    }
}

