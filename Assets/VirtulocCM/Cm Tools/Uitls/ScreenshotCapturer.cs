﻿using System;
using UnityEngine;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Virtuloc.Utils
{
    public class ScreenshotCapturer : MonoBehaviour
    {
        public int SuperSize = 8;
        public string InputPath;

        public void CaptureScreenshot()
        {
            if (string.IsNullOrEmpty(InputPath))
                InputPath = Application.dataPath;


            var name = $"screen_{Guid.NewGuid()}.png";
            var path = Path.Combine(InputPath, name);
            ScreenCapture.CaptureScreenshot(path, SuperSize);
            Debug.Log($"Screen saved: {path}");
        }
    }



#if UNITY_EDITOR
    [CustomEditor(typeof(ScreenshotCapturer))]
    public class ScreenshotCapturerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Capture Screenshot"))
            {
                var targetScript = target as ScreenshotCapturer;
                targetScript.CaptureScreenshot();
            }

        }
    }
#endif

}

