﻿using UnityEngine;

[ExecuteInEditMode]
public class FirstPersonSpawPoint : MonoBehaviour
{
    public Transform Target;

    private void Awake()
    {
        if (Target == null)
            Target = transform;
    }

#if UNITY_EDITOR

    private Color32 m_GizmoColor => new Color32(21, 246, 69, 127);

    private void OnDrawGizmos()
    {
        Gizmos.color = m_GizmoColor;
        Gizmos.DrawSphere(transform.position, .5f);
    }

#endif
}