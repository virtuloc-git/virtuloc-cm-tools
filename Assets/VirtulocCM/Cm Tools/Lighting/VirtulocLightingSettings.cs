﻿using UnityEngine;
using Virtuloc.Utils;

namespace Virtuloc.Lighting
{
    public class VirtulocLightingSettings : Singleton<VirtulocLightingSettings>
    {
        public VirtulocLightingProfile LightingProfile;

        public void ApplyProfile()
        {
            Debug.Log("Apply Lighting settings.");

            if(LightingProfile == null)
            {
                Debug.LogError("Lighting profile is null.");
                return;
            }

            RenderSettings.skybox = LightingProfile.SkyboxMaterial;

            //Enviroment Lighting
            RenderSettings.ambientMode = LightingProfile.Source;
            RenderSettings.ambientIntensity = LightingProfile.IntensityMultiplier;
            RenderSettings.ambientSkyColor = LightingProfile.SkyColor;
            RenderSettings.ambientEquatorColor = LightingProfile.EquatorColor;
            RenderSettings.ambientGroundColor = LightingProfile.GroundColor;

            //Enviroment Reflection
            RenderSettings.defaultReflectionMode = LightingProfile.ReflectionSource;
            RenderSettings.defaultReflectionResolution = LightingProfile.ReflectionResolution;
            RenderSettings.customReflection = LightingProfile.Cubemap;
            RenderSettings.reflectionIntensity = LightingProfile.ReflectionIntensityMultiplier;
            RenderSettings.reflectionBounces = LightingProfile.Bounces;

            DynamicGI.UpdateEnvironment();
        }
    }

}