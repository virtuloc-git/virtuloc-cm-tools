﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

namespace Virtuloc.Lighting
{
#if UNITY_EDITOR
    [CreateAssetMenu(fileName = "Virtuloc Lighting Profile", menuName = "Virtuloc/Lighting Profile", order = 1)]
    [ExecuteInEditMode]
    [System.Serializable]
#endif
    public class VirtulocLightingProfile : ScriptableObject
    {
        public Material SkyboxMaterial;

        [Header("Enviroment Lighting")]
        public AmbientMode Source = AmbientMode.Skybox;
        [Range(0f, 8f)]
        public float IntensityMultiplier = 1f;
        [ColorUsage(true, true)]
        public Color SkyColor = new Color(54f / 255f, 58f / 255f, 66f / 255f);
        [ColorUsage(true, true)]
        public Color EquatorColor = new Color(29f / 255f, 32f / 255f, 34f / 255f);
        [ColorUsage(true, true)]
        public Color GroundColor = new Color(12f / 255f, 11f / 255f, 9f / 255f);

        [Header("Enviroment Reflection")]
        public DefaultReflectionMode ReflectionSource;
        public int ReflectionResolution = 128;
        public Cubemap Cubemap;
        [Range(0f, 1f)]
        public float ReflectionIntensityMultiplier = 1f;
        [Range(1, 5)]
        public int Bounces = 1;

        private bool m_IsSettedUp { get; set; }




#if UNITY_EDITOR
        private void Awake()
        {
            Debug.Log($"{nameof(VirtulocLightingProfile)} succesfully created.");

            var lightingSettings = VirtulocLightingSettings.Instance;

            if (lightingSettings.LightingProfile == null)
                lightingSettings.LightingProfile = this;

            if (!m_IsSettedUp)
            {
                if (SkyboxMaterial == null)
                {
                    var skybox = Resources.Load("VirtulocDefaultSkybox") as Material;
                    SkyboxMaterial = skybox;
                }                

                m_IsSettedUp = true;

                //RenderSettings.sun
            }

        }
#endif
    }
}

