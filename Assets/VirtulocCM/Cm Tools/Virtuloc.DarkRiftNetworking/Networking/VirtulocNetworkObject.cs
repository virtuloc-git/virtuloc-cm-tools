using System;
using System.Collections.Generic;
using UnityEngine;

namespace DarkRiftNetworking
{
    public class VirtulocNetworkObject : MonoBehaviour
    {
        public string ID;
        public int OwnerID;

        public bool IsLocalPlayer() => true;
    }
}
