﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using Virtuloc.Networking;

namespace Virtuloc.Configurators
{
    [CustomEditor(typeof(MultimediaConfigurator)), CanEditMultipleObjects]
    public class MultimediaConfiguratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var configurator = (MultimediaConfigurator)target;


            if (GUILayout.Button("Generate GUID"))
            {
                (configurator).GenerateGUID();
                EditorUtility.SetDirty(target);
            }

            DrawDefaultInspector();

            if (!IsSynchronizable(configurator) ?? false)
            {
                GUILayout.Space(10f);

                if (GUILayout.Button("Make Synchronizable"))
                {
                    //AddMapPoint(map);

                    foreach (var go in Selection.gameObjects)
                    {
                        MakeSynchronizable(go);
                    }
                }
            }
        }

        private void MakeSynchronizable(GameObject go)
        {
            var configurator = go.GetComponentInChildren<MultimediaConfigurator>();

            if (configurator && (!IsSynchronizable(configurator) ?? false))
            {
                Debug.Log($"Synchronizing {go.transform.parent.name}");

                //add synchronizable to views
                foreach (var view in configurator.SlideshowSettings.PossibleViews)
                {
                    if (view.GetComponent<BaseSynchronizableObject>() == null)
                        view.gameObject.AddComponent<BaseSynchronizableObject>();

                    //add synchronizable to video
                    var video = view.GetComponentInChildren<MultimediaVideoControllerParent>();

                    if (video)
                    {
                        if (video.gameObject.GetComponent<BaseSynchronizableObject>() == null)
                            video.gameObject.AddComponent<BaseSynchronizableObject>();
                    }
                    else
                    {
                        var targets = (view as MultimediaTargetsView)?.Targets;

                        if (targets != null)
                        {
                            foreach (var target in targets)
                            {
                                if (target.GetComponent<MultimediaVideoControllerParent>() == null)
                                    target.gameObject.AddComponent<MultimediaVideoControllerParent>();

                                if (target.gameObject.GetComponent<BaseSynchronizableObject>() == null)
                                    target.gameObject.AddComponent<BaseSynchronizableObject>();
                            }
                        }
                    }

                    //fix photon views
                    foreach (var item in view.GetComponentsInChildren<Photon.Pun.PhotonView>())
                    {
                        item.OwnershipTransfer = Photon.Pun.OwnershipOption.Takeover;
                    }
                }

               
            }
        }

        private bool? IsSynchronizable(MultimediaConfigurator configurator)
        {
            if (configurator == null)
                return null;

            foreach (var item in configurator.SlideshowSettings.PossibleViews)
            {
                if(item.GetComponent<BaseSynchronizableObject>() == null)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
