﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DirectoryController
{
    public static void DirectoryCreator(string folder)
    {
        var directory = Application.dataPath.Replace("Assets", "") + folder;

        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }
    }
}
