﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JsonStreamDataController
{
    /// <summary>
    /// načtení dat ze souboru.
    /// </summary>
    /// <returns></returns>
    public static string ReadData(string nameJson)
    {
        string jsonString = "";

        using (var sr = new StreamReader(nameJson))
        {
            try
            {
                jsonString = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        return jsonString;
    }


    /// <summary>
    /// zapsání dat do souboru
    /// </summary>
    /// <param name="data"></param>
    /// <param name="nameJson"></param>
    public static void WriteData(string data, string nameJson)
    {
        using (var sw = new StreamWriter(nameJson))
        {
            try
            {
                sw.Write(data);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }
}
