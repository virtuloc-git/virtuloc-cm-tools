using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Virtuloc.Tweens
{
    [CustomEditor(typeof(Tween), true)]
    public class TweenEditor : Editor
    {
        private static readonly string[] Exclude = {"m_Script"};

        private SerializedProperty _curveProperty;
        private SerializedProperty _durationProperty;
        private SerializedProperty _loopCountProperty;
        private SerializedProperty _startDelayProperty;
        private SerializedProperty _playOnStartProperty;
        private SerializedProperty _playOnEnabledProperty;

        // Events
        private SerializedProperty _onStartProperty;
        private SerializedProperty _onEndProperty;

        private static bool _eventsFolded;

        private void OnEnable()
        {
            _curveProperty = serializedObject.FindProperty(nameof(Tween.Curve));
            _durationProperty = serializedObject.FindProperty(nameof(Tween.Duration));
            _loopCountProperty = serializedObject.FindProperty(nameof(Tween.LoopCount));
            _startDelayProperty = serializedObject.FindProperty(nameof(Tween.StartDelay));
            _playOnStartProperty = serializedObject.FindProperty(nameof(Tween.PlayOnStart));
            _playOnEnabledProperty = serializedObject.FindProperty(nameof(Tween.PlayOnEnabled));

            // Events
            _onStartProperty = serializedObject.FindProperty(nameof(Tween.OnStart));
            _onEndProperty = serializedObject.FindProperty(nameof(Tween.OnEnd));
        }

        private void OnDisable()
        {
            var script = ((Tween) target);
            if (script.Running)
            {
                script.Stop(false);
                script.ResetToStart(false);
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawButtons();
            DrawProperties();
            DrawEvents();

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            // Draw properties of derived classes
            DrawPropertiesExcluding(serializedObject, Exclude);

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawProperties()
        {
            EditorGUILayout.PropertyField(_curveProperty);
            EditorGUILayout.PropertyField(_durationProperty);
            EditorGUILayout.PropertyField(_loopCountProperty);
            EditorGUILayout.PropertyField(_startDelayProperty);
            EditorGUILayout.PropertyField(_playOnStartProperty);
            EditorGUILayout.PropertyField(_playOnEnabledProperty);
        }

        private void DrawEvents()
        {
            _eventsFolded = EditorGUILayout.Foldout(_eventsFolded, "Events", true);
            if (_eventsFolded)
            {
                EditorGUILayout.PropertyField(_onStartProperty);
                EditorGUILayout.PropertyField(_onEndProperty);
            }
        }

        private void DrawButtons()
        {
            // if (!EditorApplication.isPlaying)
            //     return;
            
            GUILayout.BeginHorizontal();

            var script = ((Tween) target);
            
            if (GUILayout.Button("Reset to start"))
            {
                script.ResetToStart();
            }

            if (GUILayout.Button("Play"))
            {
                script.Play();
            }

            if ((script.Running ? GUILayout.Button("Unpause") : GUILayout.Button("Pause")))
            {
                if(!script.Running)
                    script.Continue();
                else
                    script.Pause();
            }

            if (GUILayout.Button("Reset to end"))
            {
                script.ResetToEnd();
            }

            GUILayout.EndHorizontal();
        }
    }
}