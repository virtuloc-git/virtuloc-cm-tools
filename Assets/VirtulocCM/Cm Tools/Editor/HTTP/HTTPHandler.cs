﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;
using UnityEngine;
using UnityEngine.Networking;

public static class HTTPHandler
{
    public static string AccessToken { get; set; }

    public static void Get(string url, Action onSuccess, Action<int, string> onError, bool authorized = true)
    {
        EditorCoroutineUtility.StartCoroutineOwnerless(IE_Get(url, onSuccess, onError, authorized));
    }

    public static void Get<T>(string url, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        EditorCoroutineUtility.StartCoroutineOwnerless(IE_Get<T>(url, onSuccess, onError, authorized));
    }

    public static void PostForm<T>(string url, Dictionary<string, string> form, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        EditorCoroutineUtility.StartCoroutineOwnerless(IE_PostForm(url, form, onSuccess, onError, authorized));
    }

    public static void PostForm<T>(string url, List<IMultipartFormSection> form, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        EditorCoroutineUtility.StartCoroutineOwnerless(IE_PostForm(url, form, onSuccess, onError, authorized));
    }

    public static void PostForm<T>(string url, WWWForm form, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        EditorCoroutineUtility.StartCoroutineOwnerless(IE_PostForm(url, form, onSuccess, onError, authorized));
    }

    public static void Post<T>(string url, object data, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        EditorCoroutineUtility.StartCoroutineOwnerless(IE_Post<T>(url, data, onSuccess, onError, authorized));
    }

    private static IEnumerator IE_Get(string url, Action onSuccess, Action<int, string> onError, bool authorized = true)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            if (authorized)
            {
                ApplyAuthHeaders(request);
            }

            yield return EditorCoroutineUtility.StartCoroutineOwnerless(HandleRequest(request, onSuccess, onError));
        }
    }

    private static IEnumerator IE_Get<T>(string url, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            if (authorized)
            {
                ApplyAuthHeaders(request);
            }

            yield return EditorCoroutineUtility.StartCoroutineOwnerless(HandleRequest(request, onSuccess, onError));
        }
    }

    private static IEnumerator IE_Post<T>(string url, object data, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        using (UnityWebRequest request = new UnityWebRequest(url, UnityWebRequest.kHttpVerbPOST))
        {
            if (authorized)
            {
                ApplyAuthHeaders(request);
            }

            string json = JsonConvert.SerializeObject(data);
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(json);

            UploadHandlerRaw uH = new UploadHandlerRaw(bytes);
            DownloadHandlerBuffer dH = new DownloadHandlerBuffer();

            request.uploadHandler = uH;
            request.downloadHandler = dH;
            request.SetRequestHeader("Content-Type", "application/json");

            yield return EditorCoroutineUtility.StartCoroutineOwnerless(HandleRequest(request, onSuccess, onError));
        }
    }

    private static IEnumerator IE_PostForm<T>(string url, Dictionary<string, string> form, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            if (authorized)
            {
                ApplyAuthHeaders(request);
            }

            yield return EditorCoroutineUtility.StartCoroutineOwnerless(HandleRequest(request, onSuccess, onError));
        }
    }

    private static IEnumerator IE_PostForm<T>(string url, List<IMultipartFormSection> form, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            if (authorized)
            {
                ApplyAuthHeaders(request);
            }

            yield return EditorCoroutineUtility.StartCoroutineOwnerless(HandleRequest(request, onSuccess, onError));
        }
    }

    private static IEnumerator IE_PostForm<T>(string url, WWWForm form, Action<T> onSuccess, Action<int, string> onError, bool authorized = true)
    {
        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            if (authorized)
            {
                ApplyAuthHeaders(request);
            }

            yield return EditorCoroutineUtility.StartCoroutineOwnerless(HandleRequest(request, onSuccess, onError));
        }
    }

    private static IEnumerator HandleRequest(UnityWebRequest request, Action onSuccess, Action<int, string> onError)
    {
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            //Debug.LogError($"{request.responseCode}:{request.error}");
            onError?.Invoke((int)request.responseCode, request.error);
        }
        else
        {
            try
            {
                onSuccess?.Invoke();
            }
            catch (Exception ex)
            {
                onError?.Invoke(0, ex.Message);
            }
        }
    }

    private static IEnumerator HandleRequest<T>(UnityWebRequest request, Action<T> onSuccess, Action<int, string> onError)
    {
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            //Debug.LogError($"{request.responseCode}:{request.error}");
            onError?.Invoke((int)request.responseCode, request.error);
        }
        else
        {
            //Debug.Log(request.downloadHandler.text);
            string json = request.downloadHandler.text;
            try
            {
                var result = JsonConvert.DeserializeObject<T>(json);
                onSuccess?.Invoke(result);
            }
            catch (Exception ex)
            {
                onError?.Invoke(0, ex.Message);
            }
        }
    }

    private static IEnumerator HandleRequest(UnityWebRequest request, Action<string> onSuccess, Action<int, string> onError)
    {
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            //Debug.LogError($"{request.responseCode}:{request.error}");
            onError?.Invoke((int)request.responseCode, request.error);
        }
        else
        {
            //Debug.Log(request.downloadHandler.text);
            onSuccess?.Invoke(request.downloadHandler.text);
        }
    }

    private static void ApplyAuthHeaders(UnityWebRequest request)
    {
        request.SetRequestHeader("Authorization", $"Bearer {AccessToken}");
    }
}
