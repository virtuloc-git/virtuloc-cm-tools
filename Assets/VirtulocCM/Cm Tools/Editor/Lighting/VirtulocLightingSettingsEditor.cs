﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Virtuloc.Lighting
{
    [CustomEditor(typeof(VirtulocLightingSettings))]
    public class VirtulocLightingSettingsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            VirtulocLightingSettings lightingSettings = (VirtulocLightingSettings)target;

            DrawDefaultInspector();

            GUILayout.Space(10f);

            if (GUILayout.Button("Apply Profile"))
            {
                lightingSettings.ApplyProfile();
            }
        }
    }

}
