﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Configurator.Map
{
    [CustomEditor(typeof(Map))]
    public class MapEditor : Editor
    {     
        public override void OnInspectorGUI()
        {
            var map = (Map)target;

            DrawDefaultInspector();

            GUILayout.Space(10f);

            if (GUILayout.Button("Add Map Point"))
            {
                AddMapPoint(map);
            }
        }

        private void AddMapPoint(Map map)
        {
            var roomContainer = FindObjectOfType<RoomContainer>();

            if (roomContainer == null)
            {
                Debug.LogError("RoomContainer not found!!! Please, preppare virtuloc scene. (Click Virtuloc/Prepare Virtuloc Scene)");
                return;
            }

            var pointPrefab = Resources.Load("Prefabs/Map/[MapPoint]") as GameObject;
            var buttonPrefab = Resources.Load("Prefabs/Map/[MapButton]") as GameObject;

            var point = GameObject.Instantiate(pointPrefab, map.MapPoints);
            var button = GameObject.Instantiate(buttonPrefab, map.MapController.transform);
            var configurator = point.GetComponent<MapPointConfigurator>();
            configurator.GenerateGUID();
            var target = point.GetComponent<MapTargetModel>();
            target.Button = button.GetComponent<Button>();
            map.MapController.Targets.Add(target);

            var nullPoints = roomContainer.Points.Where(x => x == null).ToList();

            foreach (var item in nullPoints)
            {
                roomContainer.Points.Remove(item);
            }

            roomContainer.Points.Add(configurator);
        }
    }

    
}
