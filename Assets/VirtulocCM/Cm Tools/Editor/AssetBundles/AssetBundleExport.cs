﻿using System.IO;
using UnityEditor;
using UnityEngine;
using System;

public class AssetBundleExport
{
    [MenuItem("Assets/Build AssetBundles")]
    public static void BuildAllAssetBundles()
    {
        //build standalone
        string assetBundleStandaloneDirectory = "Assets/AssetBundles/standalone";

        if (!Directory.Exists(assetBundleStandaloneDirectory))
        {
            Directory.CreateDirectory(assetBundleStandaloneDirectory);
        }

        BuildPipeline.BuildAssetBundles(assetBundleStandaloneDirectory,
                                        BuildAssetBundleOptions.ForceRebuildAssetBundle,
                                        BuildTarget.StandaloneWindows);

        //build webGL
        string assetBundleWebGLDirectory = "Assets/AssetBundles/webgl";

        if (!Directory.Exists(assetBundleWebGLDirectory))
        {
            Directory.CreateDirectory(assetBundleWebGLDirectory);
        }

        BuildPipeline.BuildAssetBundles(assetBundleWebGLDirectory,
                                        BuildAssetBundleOptions.ForceRebuildAssetBundle,
                                        BuildTarget.WebGL);

        // get all names asset bundle
        var assetNames = AssetDatabase.GetAllAssetBundleNames();

        // create destination for standalone
        string destinationStandalone = "_AssetBundles/standalone";

        if (!Directory.Exists(destinationStandalone))
        {
            Directory.CreateDirectory(destinationStandalone);
        }

        // create destination for WebGL
        string destinationWebGL = "_AssetBundles/webgl";

        if (!Directory.Exists(destinationWebGL))
        {
            Directory.CreateDirectory(destinationWebGL);
        }

        // copy files
        foreach (var assetName in assetNames)
        {
            string assetBundleStandalone = "Assets/AssetBundles/standalone/" + assetName;

            File.Copy(assetBundleStandalone, destinationStandalone + "/" + assetName + "_desktop.assetbundlescene", true);

            string assetBundleWebGL = "Assets/AssetBundles/webgl/" + assetName;

            File.Copy(assetBundleWebGL, destinationWebGL + "/" + assetName + "_webgl.assetbundlescene", true);
        }
    }
}

