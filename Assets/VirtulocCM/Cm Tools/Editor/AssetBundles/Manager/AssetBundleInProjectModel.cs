﻿using Newtonsoft.Json;
using System;

[Serializable]
public class AssetBundleInProjectModel
{
    [JsonIgnore]
    public bool Select;

    [JsonIgnore]
    public bool Error;

    [JsonIgnore]
    public string ErrorMessage;

    public long ID;
    public string AssetBundleName;
}
