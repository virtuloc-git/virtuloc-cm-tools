﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using VirtulocAPI;

public class AssetBundleInProjetJsonController
{
    private static string NAME_JSON_DEV = "AssetBundleInProjectDEV.json";
    private static string NAME_JSON_RELEASE = "AssetBundleInProjectRELEASE.json";
    private static string FOLDER_JSON = "CM_Tool_Virtuloc/Data/";


    /// <summary>
    /// načtení dat ze souboru.
    /// </summary>
    /// <returns></returns>
    public static List<AssetBundleInProjectModel> LoadData()
    {
        string jsonString = "";
        var name_json = GetNameJson();

        if (File.Exists(FOLDER_JSON + name_json))
        {
            jsonString = JsonStreamDataController.ReadData(FOLDER_JSON + name_json);
        }

        return String.IsNullOrEmpty(jsonString) ? new List<AssetBundleInProjectModel>() : JsonConvert.DeserializeObject<List<AssetBundleInProjectModel>>(jsonString);
    }

    /// <summary>
    /// Uložení dat.
    /// </summary>
    /// <param name="data">Data k uložení</param>
    public static void SaveData(List<AssetBundleInProjectModel> data)
    {
        DirectoryController.DirectoryCreator(FOLDER_JSON);
        var name_json = GetNameJson();
        var json = JsonConvert.SerializeObject(data);

        JsonStreamDataController.WriteData(json, FOLDER_JSON + name_json);
    }

    public static string GetNameJson()
    {
        switch (Server.ServerType)
        {
            case ServerType.Production:
                return NAME_JSON_RELEASE;
            case ServerType.Development:
                return NAME_JSON_DEV;
            case ServerType.Custom:
                var baseURL = Server.BaseUrl.ToLower();
                baseURL = baseURL.Replace("https:", "");
                baseURL = baseURL.Replace("/", "");
                return baseURL + ".json";
            default:
                return NAME_JSON_RELEASE;
        }
    }
}
