﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VirtulocAPI;

public static class AssetBundlesVersionModule
{
    private class DownloadAssetFileVersionData
    {
        public AssetFileVersionDTO[] Data { get; set; }
        public bool Completed { get; set; }
    }

    private static Dictionary<long, DownloadAssetFileVersionData> _assetFileVersionsData;

    public static void ResetFileVersionsData(long fileID)
    {
        if (_assetFileVersionsData != null && _assetFileVersionsData.ContainsKey(fileID))
        {
            _assetFileVersionsData.Remove(fileID);
        }
    }

    private static void GetFileVersions(long assetFileID)
    {
        _assetFileVersionsData[assetFileID].Completed = false;

        VirtulocAPI.AssetBundles.GetAssetFileVersions(assetFileID,
            onSuccess: (assetFile) =>
            {
                _assetFileVersionsData[assetFileID].Data = assetFile;
                _assetFileVersionsData[assetFileID].Completed = true;
            },
            onError: (code, error) =>
            {
                _assetFileVersionsData[assetFileID].Data = null;
                _assetFileVersionsData[assetFileID].Completed = true;
            });
    }

    public static void ShowVersions( int curentVersionId , AssetBundleRuntimePlatformDTO platformData)
    {
        if (_assetFileVersionsData == null)
        {
            _assetFileVersionsData = new Dictionary<long, DownloadAssetFileVersionData>();
        }

        if (platformData != null)
        {
            if (!_assetFileVersionsData.ContainsKey(platformData.assetFileId))
            {
                _assetFileVersionsData.Add(platformData.assetFileId, new DownloadAssetFileVersionData());
                GetFileVersions(platformData.assetFileId);
            }

            if (!_assetFileVersionsData[platformData.assetFileId].Completed)
            {
                ShowWaiting();
            }
            else
            {
                var fileVersionsData = _assetFileVersionsData[platformData.assetFileId].Data;
                
                if (fileVersionsData != null)
                {
                    EditorGUILayout.LabelField("All versions:");

                    fileVersionsData = fileVersionsData.OrderByDescending(x => x.versionId).ToArray();
                    foreach (var version in fileVersionsData)
                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(version.versionId.ToString(), GUILayout.MaxWidth(20));
                            EditorGUILayout.LabelField(version.filename.ToString());
                            if (curentVersionId == version.versionId)
                            {
                                EditorGUILayout.LabelField("Curent version", GUILayout.MaxWidth(120));
                            }
                            else
                            {
                                if (GUILayout.Button("Set curent", GUILayout.MaxWidth(120)))
                                {
                                    SetVersion(version.assetFileId, version.versionId);
                                }
                            } 
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
        }
    }

    private static void SetVersion(long assetBundleID,int versionID)
    {
        VirtulocAPI.AssetBundles.SetAssetFileVersion( assetBundleID, versionID,
            onSuccess:() =>
            {
                AssetBundleDetailModule.Refresh();
                AssetBundlesManagerWindow.CallRepaint();
            },
            onError: (code, error) =>
            {
                Debug.LogError(code+" "+error);
                AssetBundleDetailModule.Refresh();
                AssetBundlesManagerWindow.CallRepaint();
            });
    }


    private static void ShowWaiting()
    {
        GUILayout.Label("Waiting for server response");
    }
}
