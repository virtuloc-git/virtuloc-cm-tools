﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEngine;
using VirtulocAPI;

public class AssetBundleUploadController
{
    private static List<AssetBundleInProjectModel> m_AssetBundleUpload = new List<AssetBundleInProjectModel>();

    private static int m_ActualListPosition;
    private static bool m_SetCurentVersion;
    private static RuntimePlatform m_ActualPlatform;

    public static void SetToUploadList(AssetBundleInProjectModel assetBundle)
    {
        m_AssetBundleUpload.Clear();
        m_AssetBundleUpload.Add(assetBundle);
        OpenDialogUpload();
    }

    public static void SetToUploadList(List<AssetBundleInProjectModel> assetBundles)
    {
        m_AssetBundleUpload.Clear();
        m_AssetBundleUpload.AddRange(assetBundles);
        OpenDialogUpload();
    }

    private static void OpenDialogUpload()
    {
        int option = EditorUtility.DisplayDialogComplex("Upload Asset Bundles",
            "You want to set this asset version as current version?",
            "Yes",
            "Close",
            "No");

        switch (option)
        {
            case 0:
                StartUploadAssetBundles(true);
                break;

            case 1:
                break;

            case 2:
                StartUploadAssetBundles(false);
                break;

            default:
                Debug.LogError("Unrecognized option.");
                break;
        }
    }

    private static void StartUploadAssetBundles(bool setCurentVersion)
    {
        m_ActualListPosition = 0;
        m_SetCurentVersion = setCurentVersion;
        m_ActualPlatform = RuntimePlatform.WebGLPlayer;
        GetRuntimePlatformsAsync();
    }

    private static void FinishUploadAssetBundle()
    {
        EditorUtility.ClearProgressBar();
        EditorUtility.DisplayDialog("Upload Asset Bundles", "Upload was successful.", "OK");

        AssetBundlesModule.Refresh();
        AssetBundlesManagerWindow.CallRepaint();
    }

    private static void StopUploadAssetBundle()
    {
        EditorUtility.ClearProgressBar();
        EditorUtility.DisplayDialog("Upload Asset Bundles", "Upload failed!!! More information in the console.", "OK");

        AssetBundlesModule.Refresh();
        AssetBundlesManagerWindow.CallRepaint();
    }

    private static void GetRuntimePlatformsAsync()
    {
        EditorUtility.DisplayProgressBar("Upload Asset Bundles", "Get Patform " + m_ActualPlatform.ToString() + " asset ID: " + m_AssetBundleUpload[m_ActualListPosition].ID.ToString(), GetPercentageProgressBar());

        VirtulocAPI.AssetBundles.GetPlatform(m_AssetBundleUpload[m_ActualListPosition].ID, m_ActualPlatform,
            onSuccess: (runtimePlatform) =>
            {
                UploadNewFile(runtimePlatform);
            },
            onError: (code, error) =>
            {
                if (code == 404)
                {
                    UploadNewFile(null);
                }
                else
                {
                    StopUploadAssetBundle();
                }
            });
    }

    private static void UploadNewFile(AssetBundleRuntimePlatformDTO platformData)
    {
        if (platformData != null)
        {
            UpdateAssetFile(platformData);
        }
        else
        {
            UploadAssetFile();
        }
    }

    private static void UploadAssetFile()
    {
        EditorUtility.DisplayProgressBar("Upload Asset Bundles", m_ActualPlatform.ToString() + " Upload new File for ID: " + m_AssetBundleUpload[m_ActualListPosition].ID.ToString(), GetPercentageProgressBar());

        var platformFilePath = AssetBundlePathGenerator.GetAssetBundlePlatformFilePath(m_ActualPlatform, m_AssetBundleUpload[m_ActualListPosition].AssetBundleName);

        if (!string.IsNullOrEmpty(platformFilePath) && File.Exists(platformFilePath))
        {
            var file = new FileInfo(platformFilePath);
            string bundleName = file.Name.Replace(file.Extension, string.Empty);

            VirtulocAPI.AssetBundles.UploadAssetFile(platformFilePath, bundleName, m_AssetBundleUpload[m_ActualListPosition].ID, m_ActualPlatform,
            onSuccess: (assetFile) =>
            {
                if (m_SetCurentVersion)
                {
                    GetFileVersions(assetFile);
                }
                else
                {
                    NextPlatform();
                }
            },
            onError: (code, error) =>
            {
                Debug.LogError(code);
                StopUploadAssetBundle();
            });

        }
        else
        {
            Debug.LogError("File "+ platformFilePath + " does not exist!");
            StopUploadAssetBundle();
        }
    }

    private static void UpdateAssetFile(AssetBundleRuntimePlatformDTO platformData)
    {
        EditorUtility.DisplayProgressBar("Upload Asset Bundles", m_ActualPlatform.ToString() + " Update File for ID: " + m_AssetBundleUpload[m_ActualListPosition].ID.ToString(), GetPercentageProgressBar());

        var platformFilePath = AssetBundlePathGenerator.GetAssetBundlePlatformFilePath(m_ActualPlatform, m_AssetBundleUpload[m_ActualListPosition].AssetBundleName);

        if (!string.IsNullOrEmpty(platformFilePath) && File.Exists(platformFilePath))
        {
            var file = new FileInfo(platformFilePath);
            string bundleName = file.Name.Replace(file.Extension, string.Empty);

            VirtulocAPI.AssetBundles.UpdateAssetFile(platformFilePath, bundleName, platformData.assetFileId,
            onSuccess: (assetFile) =>
            {
                if (m_SetCurentVersion)
                {
                    GetFileVersions(assetFile);
                }
                else
                {
                    NextPlatform();
                }
            },
            onError: (code, error) =>
            {

                Debug.LogError(code + error);
                StopUploadAssetBundle();
            });
        }
        else
        {
            Debug.LogError("File " + platformFilePath + " does not exist!");
            StopUploadAssetBundle();
        }
    }

    private static void GetFileVersions(AssetFileDTO assetFile)
    {
        EditorUtility.DisplayProgressBar("Upload Asset Bundles", m_ActualPlatform.ToString() + " Get Version for ID: " + m_AssetBundleUpload[m_ActualListPosition].ID.ToString(), GetPercentageProgressBar());

        VirtulocAPI.AssetBundles.GetAssetFileVersions(assetFile.id,
            onSuccess: (assetFileVersion) =>
            {
                if (assetFileVersion != null && assetFileVersion.Length > 0)
                {
                    SetVersion(assetFile.id, assetFileVersion[assetFileVersion.Length - 1].versionId);
                }
                else
                {
                    StopUploadAssetBundle();
                }
            },
            onError: (code, error) =>
            {
                Debug.LogError(code);
                StopUploadAssetBundle();
            });
    }

    private static void SetVersion(long assetBundleID, int versionID)
    {
        EditorUtility.DisplayProgressBar("Upload Asset Bundles", m_ActualPlatform.ToString() + " Set Version for ID: " + m_AssetBundleUpload[m_ActualListPosition].ID.ToString(), GetPercentageProgressBar());

        VirtulocAPI.AssetBundles.SetAssetFileVersion(assetBundleID, versionID,
            onSuccess: () =>
            {
                NextPlatform();
            },
            onError: (code, error) =>
            {
                Debug.LogError(code);
                StopUploadAssetBundle();
            });
    }

    private static void NextPlatform()
    {
        switch (m_ActualPlatform)
        {
            case RuntimePlatform.WebGLPlayer:
                m_ActualPlatform = RuntimePlatform.WindowsEditor;
                GetRuntimePlatformsAsync();
                break;
            case RuntimePlatform.WindowsEditor:
                NextAssetBundle();
                break;
        }
    }

    private static void NextAssetBundle()
    {
        if (m_ActualListPosition < m_AssetBundleUpload.Count - 1)
        {
            m_ActualListPosition += 1;
            m_ActualPlatform = RuntimePlatform.WebGLPlayer;
            GetRuntimePlatformsAsync();
        }
        else
        {
            FinishUploadAssetBundle();
        }
    }

    private static float GetPercentageProgressBar()
    {
        return ((m_ActualListPosition * 3.0f) + (m_ActualPlatform == RuntimePlatform.WebGLPlayer ? 1.0f : 2.0f)) / (m_AssetBundleUpload.Count * 3.0f);
    }
}