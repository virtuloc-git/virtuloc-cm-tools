﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VirtulocAPI;

public static class AssetBundlesModule
{
    private static List<AssetBundleInProjectModel> _assetBundlesInProjectJsonData = new List<AssetBundleInProjectModel>();
    private static List<AssetBundleDTO> _inProject = new List<AssetBundleDTO>();
    private static List<AssetBundleDTO> _showrooms;
    private static List<AssetBundleDTO> _models3D;
    private static bool _isGettingAssetBundles;
    private static Vector2 _scroll;
    private static int _assetBundleSelectedType;
    private static string[] _assetBundleTypes = { "InProject", "Showroom", "Model3D" };
    private static string _newAssetBundleDescription;
    private static bool _isNewAssetBundlePublic;
    private static bool _isAssetBundleCreating;
    private static List<AssetBundleDTO> _removeInProject = new List<AssetBundleDTO>();
    private static bool _selectAllInProject;
    private static string _filterAssetBundle;

    public static void Refresh()
    {
        GetAllAssetBundlesAsync();
    }

    public static void ShowAssetBundles()
    {
        if (_showrooms == null && !_isGettingAssetBundles)
        {
            GetAllAssetBundlesAsync();

        }

        if (!_isGettingAssetBundles && GUILayout.Button("Refresh"))
        {
            GetAllAssetBundlesAsync();

        }

        if (_isGettingAssetBundles)
        {
            ShowWaiting();
        }
        else
        {
            _assetBundleSelectedType = GUILayout.Toolbar(_assetBundleSelectedType, _assetBundleTypes);

            if (_assetBundleSelectedType == 0)
            {
                ShowAssetBundlesInProject();
            }
            else
            {
                ShowAssetBundles((AssetBundleType)_assetBundleSelectedType - 1);
            }

        }
    }

    private static void ShowAssetBundles(AssetBundleType type)
    {
        var bundles = GetAssetBundles(type);

        if (bundles != null)
        {
            _filterAssetBundle = EditorGUILayout.TextField("Filter:", _filterAssetBundle);
            if (!String.IsNullOrEmpty(_filterAssetBundle))
            {
                bundles = bundles.Where(x => x.description.Contains(_filterAssetBundle)).ToList();
            }

            if (bundles != null)
            {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                {
                    _scroll = GUILayout.BeginScrollView(_scroll);
                    foreach (var assetBundle in bundles)
                    {
                        ShowAssetBundle(assetBundle);
                    }
                    GUILayout.EndScrollView();
                }
                GUILayout.EndVertical();
            }
            else
            {
                GUILayout.Label("We didn't find anything");
            }
        }
        else
        {
            GUILayout.Label("Asset bundles not found");
        }


        GUILayout.Space(10);

        GUILayout.BeginVertical(EditorStyles.helpBox);
        {
            GUILayout.Label("New AssetBundle");
            _newAssetBundleDescription = EditorGUILayout.TextField("Description", _newAssetBundleDescription);
            _isNewAssetBundlePublic = EditorGUILayout.Toggle("IsPublic", _isNewAssetBundlePublic);

            if (_isAssetBundleCreating)
            {
                ShowWaiting();
            }
            else
            {
                bool isValid = !string.IsNullOrEmpty(_newAssetBundleDescription);

                GUI.enabled = isValid;
                if (GUILayout.Button("Create new"))
                {
                    CreateAssetBundleAsync(type, _newAssetBundleDescription, _isNewAssetBundlePublic);

                    _newAssetBundleDescription = string.Empty;
                    _isNewAssetBundlePublic = false;
                }

                GUI.enabled = true;
            }
        }
        GUILayout.EndVertical();
    }

    private static void ShowAssetBundle(AssetBundleDTO assetBundle)
    {
        GUILayout.BeginHorizontal(EditorStyles.helpBox);
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.LabelField("ID", assetBundle.id.ToString());
                EditorGUILayout.LabelField("Description", assetBundle.description);
                EditorGUILayout.Toggle("IsPublic", assetBundle.isPublic);

                var inProject = _inProject.Contains(assetBundle);
                inProject = EditorGUILayout.Toggle("InProject", inProject);
                if (_inProject.Contains(assetBundle) && !inProject)
                {
                    RemoveAssetBundleInProject(assetBundle);

                }
                else if (!_inProject.Contains(assetBundle) && inProject)
                {
                    AddAssetBundleInProject(assetBundle);
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            {
                if (GUILayout.Button("Detail"))
                {
                    OpenAssetBundleDetail(assetBundle);
                }
            }
            EditorGUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();
    }

    private static void RemoveAssetBundleInProject(AssetBundleDTO assetBundle)
    {
        _inProject.Remove(assetBundle);
        var assetInPoroject = _assetBundlesInProjectJsonData.Find(x => x.ID == assetBundle.id);
        if (assetInPoroject != null)
        {
            _assetBundlesInProjectJsonData.Remove(assetInPoroject);
        }
        SaveAssetBundleInProject();
    }

    private static void AddAssetBundleInProject(AssetBundleDTO assetBundle)
    {
        _inProject.Add(assetBundle);
        _assetBundlesInProjectJsonData.Add(new AssetBundleInProjectModel() { ID = assetBundle.id });
        SaveAssetBundleInProject();
    }

    private static void OpenAssetBundleDetail(AssetBundleDTO assetBundle)
    {
        AssetBundleDetailModule.AssetBundle = assetBundle;
        AssetBundleDetailModule.Reset();
    }

    private static void ShowWaiting()
    {
        GUILayout.Label("Waiting for server response");
    }

    private static List<AssetBundleDTO> GetAssetBundles(AssetBundleType type)
    {
        switch (type)
        {
            case AssetBundleType.Showroom: return _showrooms;
            case AssetBundleType.Model3D: return _models3D;
        }

        return null;
    }

    private static void GetAllAssetBundlesAsync()
    {
        _isGettingAssetBundles = true;

        VirtulocAPI.AssetBundles.GetAll(
            onSuccess: (bundles) =>
            {
                _showrooms = bundles.Where(x => x.type == AssetBundleType.Showroom && !x.isDeleted).ToList();
                _models3D = bundles.Where(x => x.type == AssetBundleType.Model3D && !x.isDeleted).ToList();
                LoadAssetBundlesInProject();
                _isGettingAssetBundles = false;
                AssetBundlesManagerWindow.CallRepaint();
            },
            onError: (code, error) =>
            {
                _showrooms = null;
                _models3D = null;
                LoadAssetBundlesInProject();
                _isGettingAssetBundles = false;
                AssetBundlesManagerWindow.CallRepaint();
            });
    }

    private static void CreateAssetBundleAsync(AssetBundleType type, string description, bool isPublic)
    {
        _isAssetBundleCreating = true;

        VirtulocAPI.AssetBundles.CreateAssetBundle(type, description, isPublic,
           onSuccess: (bundle) =>
           {
               switch (bundle.type)
               {
                   case AssetBundleType.Showroom: _showrooms.Add(bundle); break;
                   case AssetBundleType.Model3D: _models3D.Add(bundle); break;
               }

               _isAssetBundleCreating = false;
               AddAssetBundleInProject(bundle);
               OpenAssetBundleDetail(bundle);
               AssetBundlesManagerWindow.CallRepaint();
           },
           onError: (code, error) =>
           {
               _isAssetBundleCreating = false;
               AssetBundlesManagerWindow.CallRepaint();
           });
    }

    private static void LoadAssetBundlesInProject()
    {

        _assetBundlesInProjectJsonData.Clear();

        _assetBundlesInProjectJsonData = AssetBundleInProjetJsonController.LoadData();

        _inProject.Clear();

        var _allAssets = new List<AssetBundleDTO>();
        _allAssets.AddRange(_showrooms);
        _allAssets.AddRange(_models3D);

        var _assetsInProjectProblem = new List<AssetBundleInProjectModel>();

        foreach (var assetInProjectJson in _assetBundlesInProjectJsonData)
        {
            var assetBundle = _allAssets.Find(x => x.id == assetInProjectJson.ID);
            if (assetBundle != null)
            {
                _inProject.Add(assetBundle);
            }
            else
            {
                _assetsInProjectProblem.Add(assetInProjectJson);
            }
        }

        if (_assetsInProjectProblem.Count > 0)
        {
            foreach (var removeData in _assetsInProjectProblem)
            {
                _assetBundlesInProjectJsonData.Remove(removeData);
            }
            _assetsInProjectProblem.Clear();
            SaveAssetBundleInProject();
        }
    }

    private static void SaveAssetBundleInProject()
    {
        AssetBundleInProjetJsonController.SaveData(_assetBundlesInProjectJsonData);
    }

    private static void ShowAssetBundlesInProject()
    {
        var bundles = _assetBundlesInProjectJsonData;
        if (bundles.Count != 0)
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);
            {
                GUILayout.BeginHorizontal(EditorStyles.boldLabel);
                {
                    var mixedValue = _assetBundlesInProjectJsonData.Exists(x => x.Select) && _assetBundlesInProjectJsonData.Exists(x => !x.Select);
                    if (mixedValue || !_assetBundlesInProjectJsonData.Exists(x => x.Select))
                    {
                        _selectAllInProject = false;
                    }

                    EditorGUI.showMixedValue = mixedValue;

                    var selectAll = EditorGUILayout.Toggle(_selectAllInProject, GUILayout.Width(30));
                    if (selectAll != _selectAllInProject)
                    {
                        _selectAllInProject = selectAll;
                        AllSelect(selectAll);
                    }


                    EditorGUI.showMixedValue = false;

                    EditorGUILayout.LabelField("ID", GUILayout.Width(60));

                    EditorGUILayout.LabelField("Description", GUILayout.MinWidth(100));
                }
                GUILayout.EndHorizontal();

                _scroll = GUILayout.BeginScrollView(_scroll);
                foreach (var assetBundle in bundles)
                {
                    ShowAssetBundleInProject(assetBundle);
                }

                //smazání vybranýc
                if (_removeInProject.Count != 0)
                {
                    foreach (var removeAssetBundle in _removeInProject)
                    {
                        RemoveAssetBundleInProject(removeAssetBundle);
                    }
                    _removeInProject.Clear();
                }
                GUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }
        else
        {
            GUILayout.Label("Asset bundles in project not found");
        }


        GUILayout.FlexibleSpace();

        GUILayout.BeginVertical(EditorStyles.helpBox);
        {
            var _unusedNames = AssetDatabase.GetUnusedAssetBundleNames();
            var _unusedNamesError = _unusedNames.Length != 0;

            GUI.enabled = !_unusedNamesError;
            {
                if (GUILayout.Button("Export Asset Bundle"))
                {
                    AssetBundleExport.BuildAllAssetBundles();
                }
            }
            GUI.enabled = true;

            if (_unusedNamesError)
            {
                EditorGUILayout.HelpBox("Existují nepřiřazené názvy pro asset bundle", MessageType.Error);
                if (GUILayout.Button("Vyčistit Názvy"))
                {
                    AssetDatabase.RemoveUnusedAssetBundleNames();
                }
            }

            GUI.enabled = _assetBundlesInProjectJsonData.Exists(x => x.Select);
            if (GUILayout.Button("Upload all select"))
            {
                var selectList = _assetBundlesInProjectJsonData.Where(x => x.Select).ToList();
                AssetBundleUploadController.SetToUploadList(selectList);
            }
            GUI.enabled = true;
        }
        GUILayout.EndVertical();
    }

    private static void ShowAssetBundleInProject(AssetBundleInProjectModel assetBundle)
    {
        GUILayout.BeginHorizontal(EditorStyles.helpBox);
        {
            var assetDTO = _inProject.Find(x => x.id == assetBundle.ID);

            if (assetDTO != null)
            {
                GUILayout.BeginVertical();
                {
                    assetBundle.Select = EditorGUILayout.Toggle(assetBundle.Select, GUILayout.Width(30));
                }
                GUILayout.EndVertical();
                GUILayout.BeginVertical();
                {
                    GUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField(assetBundle.ID.ToString(), GUILayout.Width(60));

                        EditorGUILayout.LabelField(assetDTO.description, GUILayout.MinWidth(100));

                        if (GUILayout.Button("Unlink In Project", GUILayout.Width(125)))
                        {
                            _removeInProject.Add(assetDTO);
                        }
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    {
                        ShowAssetBundleNames(assetBundle);
                    }
                    GUILayout.EndHorizontal();

                    if (assetBundle.Error)
                    {
                        EditorGUILayout.HelpBox(assetBundle.ErrorMessage, MessageType.Error);
                    }

                    GUI.enabled = !assetBundle.Error;
                    {
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button("Upload", GUILayout.Width(100)))
                            {
                                AssetBundleUploadController.SetToUploadList(assetBundle);
                            }

                            if (GUILayout.Button("Detail", GUILayout.Width(100)))
                            {
                                OpenAssetBundleDetail(assetDTO);
                            }
                        }
                        GUILayout.EndHorizontal();
                    }
                    GUI.enabled = true;
                }
                GUILayout.EndVertical();
            }
            else
            {
                GUILayout.Label("Non-existent asset id: " + assetBundle.ID.ToString());
            }
        }
        GUILayout.EndHorizontal();
    }

    private static void ShowAssetBundleNames(AssetBundleInProjectModel assetBundle)
    {
        var names = AssetDatabase.GetAllAssetBundleNames();

        string ChangeAssetBundleName = null;
        int positionInArray = -1;
        GUILayout.BeginVertical();
        {
            GUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Bundle name: ", EditorStyles.boldLabel, GUILayout.MaxWidth(100));

                positionInArray = EditorGUILayout.Popup(Array.IndexOf(names, assetBundle.AssetBundleName), names);
            }
            GUILayout.EndHorizontal();
            if (positionInArray < 0)
            {
                ChangeAssetBundleName = null;
            }
            else
            {
                ChangeAssetBundleName = names[positionInArray];
            }

            if (ChangeAssetBundleName != assetBundle.AssetBundleName)
            {
                assetBundle.AssetBundleName = ChangeAssetBundleName;
                SaveAssetBundleInProject();
            }

            var pathAssetBundleWebGL = AssetBundlePathGenerator.GetAssetBundlePlatformFilePath(RuntimePlatform.WebGLPlayer, assetBundle.AssetBundleName);
            var pathAssetBundleDesctop = AssetBundlePathGenerator.GetAssetBundlePlatformFilePath(RuntimePlatform.WindowsEditor, assetBundle.AssetBundleName);

            if (String.IsNullOrEmpty(assetBundle.AssetBundleName))
            {
                assetBundle.Error = true;
                assetBundle.ErrorMessage = "An existing name must be selected!";
            }
            else if (!File.Exists(pathAssetBundleWebGL) || !File.Exists(pathAssetBundleDesctop))
            {
                assetBundle.Error = true;
                assetBundle.ErrorMessage = "Asset bundle files are missing. Please export them first!";
            }
            else
            {
                assetBundle.Error = false;
                assetBundle.ErrorMessage = "Clean";
            }
        }
        GUILayout.EndVertical();
    }

    private static void AllSelect(bool select)
    {
        foreach (var inProject in _assetBundlesInProjectJsonData)
        {
            inProject.Select = select;
        }
    }
}
