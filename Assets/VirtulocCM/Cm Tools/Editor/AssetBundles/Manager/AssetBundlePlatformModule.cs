﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using VirtulocAPI;

public static class AssetBundlePlatformModule
{
    private class DownloadAssetFilemData
    {
        public AssetFileDTO Data { get; set; }
        public bool Completed { get; set; }
    }

    private static Dictionary<long, DownloadAssetFilemData> _assetFilesData;
    private static List<RuntimePlatform> _uploadData = new List<RuntimePlatform>();

    public static void ResetFileData(long fileID)
    {
        if(_assetFilesData != null && _assetFilesData.ContainsKey(fileID))
        {
            _assetFilesData.Remove(fileID);
        }
    }

    public static void ShowPlatform(RuntimePlatform runtimePlatform, long bundleID, AssetBundleRuntimePlatformDTO platformData)
    {
        if(_assetFilesData == null)
        {
            _assetFilesData = new Dictionary<long, DownloadAssetFilemData>();
        }

        if (platformData != null)
        {
            if(!_assetFilesData.ContainsKey(platformData.assetFileId))
            {
                _assetFilesData.Add(platformData.assetFileId, new DownloadAssetFilemData());
                GetAssetFile(platformData.assetFileId);
            }

            if(!_assetFilesData[platformData.assetFileId].Completed)
            {
                ShowWaiting();
            }
            else
            {
                var fileData = _assetFilesData[platformData.assetFileId].Data;
                if (fileData != null)
                {
                    EditorGUILayout.LabelField("ID", fileData.id.ToString());
                    EditorGUILayout.LabelField("description", fileData.description);
                    EditorGUILayout.LabelField("currentVersion", fileData.currentVersion.ToString());
                    AssetBundlesVersionModule.ShowVersions(fileData.currentVersion, platformData);
                }

                ShowUpload(runtimePlatform, bundleID, platformData.assetFileId);
            }
        }
        else
        {
            ShowUpload(runtimePlatform, bundleID);
        }
    }

    private static void ShowWaiting()
    {
        GUILayout.Label("Waiting for server response");
    }

    private static void ShowUpload(RuntimePlatform platform, long assetBundleID = -1, long fileID = -1)
    {
        GUILayout.BeginHorizontal();
        {
            if (!_uploadData.Contains(platform))
            {
                if (GUILayout.Button("Upload new file"))
                {
                    UploadNewFile(platform, assetBundleID, fileID);
                }
            }
            else
            {
                ShowWaiting();
            }
        }
        GUILayout.EndHorizontal();
    }

    private static void UploadNewFile(RuntimePlatform platform, long assetBundleID = -1, long fileID = -1)
    {
        var path = EditorUtility.OpenFilePanelWithFilters("Select Asset Bundle", Application.dataPath, new string[]{ "Asset Bundles", "assetbundlescene", "All files", "*" });
        if(!string.IsNullOrEmpty(path))
        {
            if (fileID >= 0)
            {
                UpdateAssetFile(platform, path, fileID);
            }
            else
            {
                UploadAssetFile(platform, path, assetBundleID);
            }
        }
    }

    private static void GetAssetFile(long assetFileID)
    {
        _assetFilesData[assetFileID].Completed = false;

        VirtulocAPI.AssetBundles.GetAssetFile(assetFileID,
            onSuccess: (assetFile) =>
            {
                _assetFilesData[assetFileID].Data = assetFile;
                _assetFilesData[assetFileID].Completed = true;
            },
            onError: (code, error) =>
            {
                _assetFilesData[assetFileID].Data = null;
                _assetFilesData[assetFileID].Completed = true;
            });
    }

    private static void UploadAssetFile(RuntimePlatform platform, string path, long assetBundleID)
    {
        var fi = new FileInfo(path);
        string bundleName = fi.Name.Replace(fi.Extension, string.Empty);

        _uploadData.Add(platform);

        VirtulocAPI.AssetBundles.UploadAssetFile(path, bundleName, assetBundleID, platform,
            onSuccess: (assetFile) =>
            {
                if (!_assetFilesData.ContainsKey(assetFile.id))
                {
                    _assetFilesData.Add(assetFile.id, new DownloadAssetFilemData());
                }

                _assetFilesData[assetFile.id].Data = assetFile;
                _assetFilesData[assetFile.id].Completed = true;

                _uploadData.Remove(platform);
                AssetBundleDetailModule.Refresh();
                AssetBundlesManagerWindow.CallRepaint();
            },
            onError: (code, error) =>
            {
                _uploadData.Remove(platform);
                AssetBundlesManagerWindow.CallRepaint();
            });
    }

    private static void UpdateAssetFile(RuntimePlatform platform, string path, long assetFileID)
    {
        var fi = new FileInfo(path);
        string bundleName = fi.Name.Replace(fi.Extension, string.Empty);

        _uploadData.Add(platform);

        VirtulocAPI.AssetBundles.UpdateAssetFile(path, bundleName, assetFileID,
            onSuccess: (assetFile) =>
            {
                if (!_assetFilesData.ContainsKey(assetFile.id))
                {
                    _assetFilesData.Add(assetFile.id, new DownloadAssetFilemData());
                }
                else
                {
                    assetFile.description = _assetFilesData[assetFile.id].Data.description;
                }

                _assetFilesData[assetFile.id].Data = assetFile;
                _assetFilesData[assetFile.id].Completed = true;

                _uploadData.Remove(platform);
                AssetBundlesManagerWindow.CallRepaint();
            },
            onError: (code, error) =>
            {
                _uploadData.Remove(platform);
                AssetBundlesManagerWindow.CallRepaint();
            });
    }
}
