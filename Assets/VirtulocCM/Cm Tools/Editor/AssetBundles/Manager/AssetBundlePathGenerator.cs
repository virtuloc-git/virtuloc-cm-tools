﻿using UnityEngine;

public class AssetBundlePathGenerator
{
    public static string GetAssetBundlePlatformFilePath(RuntimePlatform runtimePlatform,string assetBundleName)
    {
        switch (runtimePlatform)
        {
            case RuntimePlatform.WebGLPlayer:
                return Application.dataPath.Replace("/Assets", "/_AssetBundles/webgl/" + assetBundleName + "_webgl.assetbundlescene");
            case RuntimePlatform.WindowsEditor:
                return Application.dataPath.Replace("/Assets", "/_AssetBundles/standalone/" + assetBundleName + "_desktop.assetbundlescene");
            default:
                return Application.dataPath.Replace("/Assets", "/_AssetBundles/standalone/" + assetBundleName + "_desktop.assetbundlescene");
        }
    }
}