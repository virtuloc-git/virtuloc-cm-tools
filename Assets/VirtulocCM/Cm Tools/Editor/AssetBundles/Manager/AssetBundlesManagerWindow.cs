﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VirtulocAPI;

public class AssetBundlesManagerWindow : EditorWindow
{
    [MenuItem("Virtuloc/Asset Bundles")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        var window = (AssetBundlesManagerWindow)EditorWindow.GetWindow(typeof(AssetBundlesManagerWindow));
        window.minSize = new Vector2(320, 320);
        window.titleContent = new GUIContent("Asset Bundles");
        window.Initialize();
        window.Show();
    }

    public static void CallRepaint()
    {
        EditorWindow.GetWindow(typeof(AssetBundlesManagerWindow)).Repaint();
    }

    private const string m_LoginPrefsKey = "LOGIN";
    private const string m_PasswordPrefsKey = "PWD";
    private const string m_ServerTypePrefsKey = "SERVERTYPE";
    private const string m_ServerURLPrefsKey = "SERVERURL";

    private bool m_IsAuthorized => HTTPHandler.AccessToken != null;

    private string m_CustomServerURL;
    private string m_Login;
    private string m_Password;
    private bool m_IsAuthorizing;
    private ServerType m_ServerType;

    private AssetBundleDTO m_SelectedAssetBundle;

    public void Initialize()
    {
        LoadPlayerPrefs();

        if (!m_IsAuthorized && !string.IsNullOrEmpty(m_Login) && !string.IsNullOrEmpty(m_Password))
        {
            AuthorizeAsync();
        }
    }

    private void OnGUI()
    {
        if (string.IsNullOrEmpty(m_Login) && string.IsNullOrEmpty(m_Password) && string.IsNullOrEmpty(m_CustomServerURL) && m_ServerType == ServerType.Production)
        {
            LoadPlayerPrefs();
        }

        if (!m_IsAuthorized)
        {
            if (m_IsAuthorizing)
            {
                ShowWaiting();
            }
            else
            {
                ShowLogin();
            }
        }
        else
        {
            ShowLogout();
            GUILayout.Space(10);

            if (AssetBundleDetailModule.AssetBundle != null)
            {
                AssetBundleDetailModule.ShowAssetBundleDetail();
            }
            else
            {
                AssetBundlesModule.ShowAssetBundles();
            }
        }
    }

    private void LoadPlayerPrefs()
    {
        m_CustomServerURL = EditorPrefs.GetString(m_ServerURLPrefsKey, null);
        m_Login = EditorPrefs.GetString(m_LoginPrefsKey, null);
        m_Password = EditorPrefs.GetString(m_PasswordPrefsKey, null);
        m_ServerType = (ServerType)EditorPrefs.GetInt(m_ServerTypePrefsKey, 0);
    }

    private void ShowWaiting()
    {
        GUILayout.Label("Waiting for server response");
    }

    private void ShowLogin()
    {
        m_Login = EditorGUILayout.TextField("Login", m_Login);
        m_Password = EditorGUILayout.TextField("Password", m_Password);

        m_ServerType = (ServerType)EditorGUILayout.EnumPopup("Server Type", m_ServerType);

        if (m_ServerType == ServerType.Custom)
        {
            m_CustomServerURL = EditorGUILayout.TextField("Custom Server URL", m_CustomServerURL);
        }

        if(GUILayout.Button("Login"))
        {
            AuthorizeAsync();
        }
    }

    private void ShowLogout()
    {
        if (GUILayout.Button($"Logout ({ m_ServerType.ToString()})"))
        {
            HTTPHandler.AccessToken = null;
        }
    }

    private void AuthorizeAsync()
    {
        VirtulocAPI.Server.SetEnvironment(m_ServerType,m_CustomServerURL);

        m_IsAuthorizing = true;

        VirtulocAPI.Auth.GetAccessToken(m_Login, m_Password,
                onSuccess: () =>
                {
                    EditorPrefs.SetString(m_LoginPrefsKey, m_Login);
                    EditorPrefs.SetString(m_PasswordPrefsKey, m_Password);
                    EditorPrefs.SetInt(m_ServerTypePrefsKey, (int) m_ServerType);
                    EditorPrefs.SetString(m_ServerURLPrefsKey, m_CustomServerURL);
                    m_IsAuthorizing = false;
                    AssetBundlesModule.Refresh();
                    Repaint();
                },
                onError: (code, error) =>
                {
                    EditorPrefs.DeleteKey(m_LoginPrefsKey);
                    EditorPrefs.DeleteKey(m_PasswordPrefsKey);
                    m_IsAuthorizing = false;
                    Repaint();
                });
    }
}