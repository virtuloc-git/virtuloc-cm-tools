﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VirtulocAPI;

public static class AssetBundleDetailModule
{
    private class DownloadRuntimePlatformData
    {
        public AssetBundleRuntimePlatformDTO Data { get; set; }
        public bool Completed { get; set; }
    }

    public static AssetBundleDTO AssetBundle { get; set; }

    private static Dictionary<RuntimePlatform, DownloadRuntimePlatformData> _runtimePlatformsData;
    private static Vector2 _scroll;

    public static void InitializeRuntimePlatforms()
    {
        _runtimePlatformsData = new Dictionary<RuntimePlatform, DownloadRuntimePlatformData>()
        {
            { RuntimePlatform.WebGLPlayer, new DownloadRuntimePlatformData() },
            { RuntimePlatform.WindowsEditor, new DownloadRuntimePlatformData() },
        };
    }

    public static void Reset()
    {
        _runtimePlatformsData = null;
    }

    public static void Refresh()
    {
        if (_runtimePlatformsData != null)
        {
            foreach (var kvp in _runtimePlatformsData)
            {
                GetRuntimePlatformsAsync(AssetBundle.id, kvp.Key);
                AssetBundlesVersionModule.ResetFileVersionsData(kvp.Value.Data.assetFileId);
            }
        }
    }

    public static void ShowAssetBundleDetail()
    {
        if (GUILayout.Button("Back"))
        {
            AssetBundle = null;
        }

        if (AssetBundle != null)
        {
            if (_runtimePlatformsData == null)
            {
                InitializeRuntimePlatforms();

                foreach (var kvp in _runtimePlatformsData)
                {
                    GetRuntimePlatformsAsync(AssetBundle.id, kvp.Key);
                }
            }

            EditorGUILayout.LabelField("ID", AssetBundle.id.ToString());
            EditorGUILayout.LabelField("Description", AssetBundle.description);
            EditorGUILayout.Toggle("IsPublic", AssetBundle.isPublic);

            if (GUILayout.Button("Refresh"))
            {
                Refresh();
            }

            _scroll = GUILayout.BeginScrollView(_scroll);
            {
                foreach (var kvp in _runtimePlatformsData)
                {
                    ShowRuntimePlatform(kvp.Key);
                }
            }
            GUILayout.EndScrollView();
        }
    }

    private static void ShowRuntimePlatform(RuntimePlatform platform)
    {
        GUILayout.BeginVertical(EditorStyles.helpBox);
        {
            EditorGUILayout.LabelField("Platform", platform.ToString());

            if (_runtimePlatformsData[platform].Completed)
            {
                AssetBundlePlatformModule.ShowPlatform(platform, AssetBundle.id, _runtimePlatformsData[platform].Data);
            }
            else
            {
                ShowWaiting();
            }
        }
        GUILayout.EndVertical();
    }

    private static void ShowWaiting()
    {
        GUILayout.Label("Waiting for server response");
    }

    private static void GetRuntimePlatformsAsync(long assetBundleID, RuntimePlatform platform)
    {
        _runtimePlatformsData[platform].Completed = false;

        VirtulocAPI.AssetBundles.GetPlatform(assetBundleID, platform,
            onSuccess: (runtimePlatform) =>
            {
                _runtimePlatformsData[platform].Data = runtimePlatform;
                _runtimePlatformsData[platform].Completed = true;

                AssetBundlePlatformModule.ResetFileData(_runtimePlatformsData[platform].Data.assetFileId);
                AssetBundlesManagerWindow.CallRepaint();
            },
            onError: (code, error) =>
            {
                _runtimePlatformsData[platform].Data = null;
                _runtimePlatformsData[platform].Completed = true;
                AssetBundlesManagerWindow.CallRepaint();
            });
    }
}
