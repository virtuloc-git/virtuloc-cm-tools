﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Virtuloc.CM
{
    [InitializeOnLoad]
    public static class AutoUpdater
    {
        private static readonly string VERSION = "0.47.0";

        private class Package
        {
            public Package(string manifestLine, int packageIndex)
            {
                ManifestLine = manifestLine;
                PackageIndex = packageIndex;
            }

            public string ManifestLine;
            public int PackageIndex;
        }

        private class ManifestModel
        {
            public Dictionary<string, string> dependencies { get; set; }
        }

        #region Properties
        private static string ManifestPath
        {
            get
            {
                var projectPath = Directory.GetParent(Application.dataPath).FullName;
                var manifestPath = Path.Combine(projectPath, "Packages", "manifest.json");
                return manifestPath;
            }
        }

        private static string PackagesLockPath
        {
            get
            {
                var projectPath = Directory.GetParent(Application.dataPath).FullName;
                var packagesLockPath = Path.Combine(projectPath, "Packages", "packages-lock.json");
                return packagesLockPath;
            }
        }

        private const string VIRTULOC_KEY = "com.virtuloc.cm-tools";
        private const string VIRTULOC_GIT_URL = "https://bitbucket.org/virtuloc-git/virtuloc-cm-tools.git?path=/Assets/VirtulocCM";
        #endregion

        #region Fields
        private static List<Package> packages = new List<Package>();
        private static bool verbose = false;
        private static DateTime m_StartTime;

        #endregion

        // TODO: AUTO UPDATE

        static AutoUpdater()
        {
            m_StartTime = DateTime.Now;
            EditorApplication.update += Timer;
        }

        private static void Timer()
        {
            if ((DateTime.Now - m_StartTime).TotalSeconds > 1)
            {
                EditorApplication.update -= Timer;
                CheckUpdates();
            }
        }

        [MenuItem("Virtuloc/Check Updates", false, 3)]
        private static void CheckUpdatesMenu()
        {
            //ReinstallVirtulocPackage(true);
            CheckLatestVersion();
        }

        [MenuItem("Virtuloc/Force Update", false, 4)]
        private static void ForceUpdateMenu()
        {
            ReinstallVirtulocPackage(true);
        }

        private static void CheckUpdates()
        {
            //ReinstallVirtulocPackage();
            CheckLatestVersion();
        }

        private class PackageModel
        {
            public string version;
        }

        private static void CheckLatestVersion()
        {
            //return;

            //var url = @"https://bitbucket.org/virtuloc-git/virtuloc-cm-tools/raw/7aa29510edb55374e41a312ce14a76bcea857fee/Assets/VirtulocCM/package.json";
            var url = @"https://bitbucket.org/virtuloc-git/virtuloc-cm-tools/raw/master/Assets/VirtulocCM/package.json";

            var webR = new WebRequestInEditor(url, resp =>
            {
                var package = JsonUtility.FromJson<PackageModel>(resp);

                if (package.version == VERSION)
                {
                    Debug.Log($"Virtuloc content tool is up to date (version: {VERSION}).");
                }
                else
                {
                    Debug.Log($"New version of Virtuloc content tool found (version: {package.version}).");

                    //Debug.Log("Updating Virtuloc content tool...");
                    //ReinstallVirtulocPackage(true);

                    if (EditorUtility.DisplayDialog("Virtuloc CM Tool Updater", $"New Version of Virtuloc CM Tool found ({package.version}). Do you want to update?", "Update", "Not now"))
                    {
                        Debug.Log("Updating Virtuloc content tool...");
                        ReinstallVirtulocPackage(true);
                    }
                    else
                    {
                        return;
                    }
                }
            });

            webR.Send();

        }

        //public static void RefreshPackages()
        //{
        //    if (verbose) Debug.Log("RefreshPackages");

        //    packages.Clear();

        //    var manifestLines = File.ReadAllText(ManifestPath).Split('\n');

        //    for (var i = 0; i < manifestLines.Length; i++)
        //    {
        //        var manifestLine = manifestLines[i];
        //        if (manifestLine.Contains("com."))
        //        {
        //            manifestLine = manifestLine.Split(':')[0];
        //            manifestLine = manifestLine.Replace("    \"", string.Empty);
        //            manifestLine = manifestLine.Replace("\"", string.Empty);
        //            manifestLine = manifestLine.Replace("com.", string.Empty);
        //            packages.Add(new Package(manifestLine, i));
        //        }
        //    }
        //}

        private static void ReinstallVirtulocPackage(bool force = false)
        {
#if NOT_INSTALL_VIRTULOC
            return;
#endif

            var time = DateTime.Now;

            var lastUpdateString = PlayerPrefs.GetString("LastVirtulocUpdate");

            DateTime lastUpdatedTime;

            if (!DateTime.TryParse(lastUpdateString, out lastUpdatedTime))
                force = true;

            if (!force)
            {
                if ((time - lastUpdatedTime).TotalMinutes < 10)
                    return;
            }

            Debug.Log("Checking for Virtuloc updates.");

            PlayerPrefs.SetString("LastVirtulocUpdate", time.ToString());

            UpdateCMTool();

            return;

            //RefreshPackages();

            //var package = packages.FirstOrDefault(x => x.ManifestLine.Contains(VIRTULOC_KEY.Replace("com.", "")));
            //var atLine = package?.PackageIndex ?? -1;

            //if (atLine <= -1)
            //{
            //    InstallVirtulocPackage();

            //    return;
            //}

            //if (verbose) Debug.Log(string.Format("ReinstallPackage ( atLine: {0} )", atLine));

            //var manifestLines = File.ReadAllText(ManifestPath).Split('\n');
            //var oldManifest = string.Join("\n", manifestLines);
            //ArrayUtility.RemoveAt(ref manifestLines, atLine);
            //var newManifest = string.Join("\n", manifestLines);
            //File.WriteAllText(ManifestPath, newManifest);
            //AssetDatabase.Refresh();
            //File.WriteAllText(ManifestPath, oldManifest);
            //AssetDatabase.Refresh();
        }

        private static float _updateRes = 0.0005f;

        private static void UpdateCMTool()
        {
            _updateRes = 0.0005f;

            var request = UnityEditor.PackageManager.Client.Add(VIRTULOC_GIT_URL);

            _updateCMToolCoroutineAction = () => UpdateCMToolCoroutine(request);
            EditorApplication.update += _updateCMToolCoroutineAction;

        }

        private static EditorApplication.CallbackFunction _updateCMToolCoroutineAction = null;

        private static void UpdateCMToolCoroutine(UnityEditor.PackageManager.Requests.AddRequest request)
        {
            while (!request.IsCompleted)
            {
                if (_updateRes < 1f)
                    EditorUtility.DisplayProgressBar("Virtuloc", "Updating Virtuloc Content tool...", Mathf.Clamp01(_updateRes));
                else
                    EditorUtility.DisplayProgressBar("Virtuloc", "Finalizing update of Virtuloc Content tool...", 1f);

                if (_updateRes <= 1f)
                    _updateRes += 0.0005f;

                return;
            }

            EditorApplication.update -= _updateCMToolCoroutineAction;
            _updateCMToolCoroutineAction = null;

            EditorUtility.ClearProgressBar();

            AssetDatabase.Refresh();

            if (request.Status == UnityEditor.PackageManager.StatusCode.Success)
            {
                Debug.Log("Virtuloc package updated.");
            }
            else
            {
                Debug.LogError("Virtuloc package has not been updated.");
            }
        }

        //private static void InstallVirtulocPackage()
        //{
        //    var manifestLines = File.ReadAllText(ManifestPath).Split('\n').ToList();

        //    if (manifestLines.Any(x => x.Contains(VIRTULOC_KEY)))
        //        return;

        //    for (int i = 0; i < manifestLines.Count - 1; i++)
        //    {
        //        if (!manifestLines[i].Contains("{"))
        //        {
        //            manifestLines.Insert(i, $"\"{VIRTULOC_KEY}\": \"{VIRTULOC_GIT_URL}\",");
        //            break;
        //        }
        //    }

        //    var sb = new StringBuilder();

        //    foreach (var item in manifestLines)
        //    {
        //        sb.Append(item).Append('\n');
        //    }

        //    File.WriteAllText(ManifestPath, sb.ToString());

        //    AssetDatabase.Refresh();
        //}
    }

    public class WebRequestInEditor
    {
        private Action<string> _callback;
        // Lets poke Maps for the distance between two cities.
        UnityEngine.Networking.UnityWebRequest www;
        string _url;

        public WebRequestInEditor(string url, Action<string> callback)
        {
            _url = url;
            _callback = callback;
        }

        void Request()
        {
            www = UnityEngine.Networking.UnityWebRequest.Get(_url);
            www.SendWebRequest();
        }

        void EditorUpdate()
        {
            if (!www.isDone)
                return;

            if (www.isNetworkError)
                Debug.Log(www.error);
            else
            {
                _callback?.Invoke(www.downloadHandler.text);
            }

            EditorApplication.update -= EditorUpdate;
        }

        public void Send()
        {
            Request();
            EditorApplication.update += EditorUpdate;
        }
    }
}
