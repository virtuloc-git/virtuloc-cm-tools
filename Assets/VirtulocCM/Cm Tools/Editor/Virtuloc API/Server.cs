﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtulocAPI
{
    public static class Server
    {
        private const string m_BASE_URL_DEV = "https://dev-admin.virtuloc.com";
        private const string m_BASE_URL = "https://admin.virtuloc.com";
        private static string m_CustomURL;

        public static ServerType ServerType {  get; private set; }

        public static string BaseUrl
        {
            get
            {
                switch (ServerType)
                {
                    case ServerType.Production:
                        return m_BASE_URL;
                    case ServerType.Development:
                        return m_BASE_URL_DEV;
                    case ServerType.Custom:
                        return m_CustomURL;
                    default:
                        return m_BASE_URL;
                }
            }
        }

        public static void SetEnvironment(ServerType serverType, string customURL = "")
        {
            ServerType = serverType;
            m_CustomURL = customURL;
        }
    }
}
