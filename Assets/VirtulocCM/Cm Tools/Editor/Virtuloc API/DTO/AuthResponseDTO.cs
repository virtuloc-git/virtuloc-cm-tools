﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtulocAPI
{
    public class AuthResponseDTO
    {
        public string access_token { get; set; }
    }
}