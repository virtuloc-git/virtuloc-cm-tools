﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtulocAPI
{
    public class AssetFileDTO
    {
        public long id { get; set; }
        public int currentVersion { get; set; }
        public string description { get; set; }
    }
}
