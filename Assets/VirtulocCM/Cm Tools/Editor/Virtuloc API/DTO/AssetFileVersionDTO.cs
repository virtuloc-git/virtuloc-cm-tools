﻿using System;

namespace VirtulocAPI
{
    [Serializable]
    public class AssetFileVersionDTO
    {
        public long assetFileId;
        public int versionId;
        public string filename;
        public string contentType;
        public int contentLength;
        public int createdByUserId;
        public int updatedByUserId;
        public string updatedDate;
        public string createdDate;
    }
}
