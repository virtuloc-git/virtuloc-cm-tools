﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtulocAPI
{
    public class AssetBundleDTO
    {
        public long id { get; set; }
        public string description { get; set; }
        public AssetBundleType type { get; set; }
        public bool isPublic { get; set; }
        public bool isDeleted { get; set; }
    }
}