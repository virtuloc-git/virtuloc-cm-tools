﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtulocAPI
{
    public class AssetBundleRuntimePlatformDTO
    {
        public long assetBundleId { get; set; }
        public RuntimePlatform runtimePlatformId { get; set; }
        public long assetFileId { get; set; }
    }
}
