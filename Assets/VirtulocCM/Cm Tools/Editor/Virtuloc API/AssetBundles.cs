﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace VirtulocAPI
{
    public class AssetBundles
    {
        public static void GetAll(Action<List<AssetBundleDTO>> onSuccess, Action<int, string> onError)
        {
            string url = $"{Server.BaseUrl}/api/AssetBundle";
            HTTPHandler.Get<List<AssetBundleDTO>>(url, onSuccess, onError);
        }

        public static void CreateAssetBundle(AssetBundleType type, string description, bool isPublic, Action<AssetBundleDTO> onSuccess, Action<int, string> onError)
        {
            AssetBundleDTO data = new AssetBundleDTO()
            {
                description = description,
                isPublic = isPublic,
                type = type,
            };

            string url = $"{Server.BaseUrl}/api/AssetBundle";
            HTTPHandler.Post<AssetBundleDTO>(url, data, onSuccess, onError);
        }

        public static void GetPlatform(long assetBundleID, RuntimePlatform platform, Action<AssetBundleRuntimePlatformDTO> onSuccess, Action<int, string> onError)
        {
            string url = $"{Server.BaseUrl}/api/AssetBundleRuntimePlatform/({assetBundleID},{platform})";
            HTTPHandler.Get<AssetBundleRuntimePlatformDTO>(url, onSuccess, onError);
        }

        public static void GetAssetFile(long fileID, Action<AssetFileDTO> onSuccess, Action<int, string> onError)
        {
            string url = $"{Server.BaseUrl}/api/AssetFile/{fileID}";
            HTTPHandler.Get<AssetFileDTO>(url, onSuccess, onError);
        }

        public static void GetAssetFileVersions(long fileID, Action<AssetFileVersionDTO[]> onSuccess, Action<int, string> onError)
        {
            string url = $"{Server.BaseUrl}/api/AssetFile/getVersions/{fileID}";
            HTTPHandler.Get<AssetFileVersionDTO[]>(url, onSuccess, onError);
        }

        public static void SetAssetFileVersion(long fileID, int versionID, Action onSuccess, Action<int, string> onError)
        {
            string url = $"{Server.BaseUrl}/api/AssetFile/setVersion({fileID},{versionID})";
            HTTPHandler.Get(url, onSuccess, onError);
        }

        public static void UploadAssetFile(string filePath, string description, long assetBundleID, RuntimePlatform platform, Action<AssetFileDTO> onSuccess, Action<int, string> onError)
        {
            var fileData = GetFileData(filePath);
            var fileInfo = new FileInfo(filePath);

            WWWForm formData = new WWWForm();
            formData.AddField("description", description);
            formData.AddField("assetBundleId", (int)assetBundleID);
            formData.AddField("runtimePlatformId", (int)platform);
            formData.AddBinaryData("file", fileData, fileInfo.Name);

            string url = $"{Server.BaseUrl}/api/assetfile/upload";
            HTTPHandler.PostForm<AssetFileDTO>(url, formData, onSuccess, onError);
        }

        public static void UpdateAssetFile(string filePath, string description, long assetFileID, Action<AssetFileDTO> onSuccess, Action<int, string> onError)
        {
            var fileData = GetFileData(filePath);
            var fileInfo = new FileInfo(filePath);

            WWWForm formData = new WWWForm();
            formData.AddField("description", description);
            formData.AddField("assetFileId", (int)assetFileID);
            formData.AddBinaryData("file", fileData, fileInfo.Name);

            string url = $"{Server.BaseUrl}/api/assetfile/upload";
            HTTPHandler.PostForm<AssetFileDTO>(url, formData, onSuccess, onError);
        }

        private static byte[] GetFileData(string path)
        {
            return File.ReadAllBytes(path);
        }
    }
}
