﻿namespace VirtulocAPI
{
    public enum ServerType
    {
        Production = 0,
        Development = 1,
        Custom = 2,
    }
}

