﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtulocAPI
{
    public static class Auth
    {
        public static void GetAccessToken(string user, string pwd, Action onSuccess, Action<int, string> onError)
        {
            string url = $"{Server.BaseUrl}/connect/token";

            var formData = new Dictionary<string, string>()
            {
                { "username", user },
                { "password", pwd },
                { "grant_type", "password" },
                { "client_id", "swaggerui" },
                { "client_secret", "q$27b12665847c650222bdbeaf>Zz3M" },
                { "scope", "virtuloc_api" }
            };

            HTTPHandler.PostForm<AuthResponseDTO>(url, formData, 
                onSuccess: (authDTO) =>
                {
                    HTTPHandler.AccessToken = authDTO.access_token;
                    onSuccess?.Invoke();
                },
                onError, authorized: false);
        }
    }
}
