﻿using Configurator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Virtuloc.CM
{
    public class VirtulocEditor
    {
        [MenuItem("Virtuloc/Prepare Virtuloc Scene",false,1)]
        private static void PrepareScene()
        {
            var interactible = Resources.Load("Prefabs/Interectible") as GameObject;
            if (interactible != null)
            {
                var go = GameObject.Instantiate(interactible);
                go.name = "Interactible";
                go.transform.localPosition = Vector3.zero;
                go.transform.rotation = Quaternion.identity;
                go.transform.localScale = Vector3.one;
            }
            else
            {
                Debug.LogError("Interactible prefab not found.");
            }
        }

        #region [Configurators]

        //[MenuItem("Virtuloc/Configurators/Create 2D Configurator Old", false, 21)]
        [Obsolete("Use CreateMultimediaConfigurator isntead.",true)]
        private static void Create2DConfigurator()
        {
            var prefab = Resources.Load("Prefabs/2D Configurator") as GameObject;
            if (prefab != null)
            {
                var go = GameObject.Instantiate(prefab);

                go.name = CreateConfiguratorName(go.name);

                var configurator = go.GetComponentInChildren<BaseContainer>();

                SetupConfigurator(configurator);
            }
            else
            {
                Debug.LogError("Configurator prefab not found.");
            }
        }

        [MenuItem("Virtuloc/Configurators/Create 2D Configurator", false, 21)]
        private static void CreateMultimediaConfigurator()
        {
            var prefab = Resources.Load("Prefabs/MultimediaConfigurator/[MultimediaConfiguratorNoViews]") as GameObject;
            if (prefab != null)
            {
                var go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;


                go.name = "[MultimediaConfigurator]";
                go.name = CreateConfiguratorName(go.name);

                var configurator = go.GetComponentInChildren<BaseContainer>();

                // create views
                var multimediaConfigurator = configurator as Configurators.MultimediaConfigurator;

                //bar view
                var barPrefab = Resources.Load("Prefabs/MultimediaConfigurator/[BarView]") as GameObject;

                if (barPrefab)
                {
                    var barGo = PrefabUtility.InstantiatePrefab(barPrefab, multimediaConfigurator.transform.parent) as GameObject;

                    multimediaConfigurator.SlideshowSettings.PossibleViews.Add(barGo.GetComponent<Configurators.BaseMultimediaTargetsView>());
                }

                //single
                var singlePrefab = Resources.Load("Prefabs/MultimediaConfigurator/[SingleView]") as GameObject;

                if (singlePrefab)
                {
                    var singleGo = PrefabUtility.InstantiatePrefab(singlePrefab, multimediaConfigurator.transform.parent) as GameObject;

                    multimediaConfigurator.SlideshowSettings.PossibleViews.Add(singleGo.GetComponent<Configurators.BaseMultimediaTargetsView>());
                }

                //multi
                var multiPrefab = Resources.Load("Prefabs/MultimediaConfigurator/[MultiView]") as GameObject;

                if (multiPrefab)
                {
                    var multiGo = PrefabUtility.InstantiatePrefab(multiPrefab, multimediaConfigurator.transform.parent) as GameObject;

                    multimediaConfigurator.SlideshowSettings.PossibleViews.Add(multiGo.GetComponent<Configurators.BaseMultimediaTargetsView>());
                }

                SetupConfigurator(configurator);
            }
            else
            {
                Debug.LogError("Configurator prefab not found.");
            }
        }

      //  [MenuItem("Virtuloc/Configurators/Create 2D Configurator Old", false, 21)]
        private static void CreateMultimediaConfiguratorOld()
        {
            var prefab = Resources.Load("Prefabs/MultimediaConfigurator/[MultimediaConfiguratorOld]") as GameObject;
            if (prefab != null)
            {
                var go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                go.name = CreateConfiguratorName(go.name);

                var configurator = go.GetComponentInChildren<BaseContainer>();               

                SetupConfigurator(configurator);
            }
            else
            {
                Debug.LogError("Configurator prefab not found.");
            }
        }

        [MenuItem("Virtuloc/Configurators/Create 3D Configurator", false, 21)]
        private static void Create3DConfigurator()
        {
            var prefab = Resources.Load("Prefabs/[3D Configurator]") as GameObject;
            if (prefab != null)
            {
                var go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                go.name = CreateConfiguratorName(go.name);

                var configurator = go.GetComponentInChildren<BaseContainer>();              

                SetupConfigurator(configurator);
            }
            else
            {
                Debug.LogError("Configurator prefab not found.");
            }
        }

        [MenuItem("Virtuloc/Configurators/Create Text Configurator", false, 22)]
        private static void CreateTextConfigurator()
        {
            var prefab = Resources.Load("Prefabs/TextConfigurator/[TextConfigurator]") as GameObject;
            if (prefab != null)
            {
                var go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                go.name = "[TextConfigurator]";
                go.name = CreateConfiguratorName(go.name);

                var configurator = go.GetComponentInChildren<BaseContainer>();
              
                SetupConfigurator(configurator);
            }
            else
            {
                Debug.LogError("Configurator prefab not found.");
            }
        }

        [MenuItem("Virtuloc/Configurators/Create MeshRenderer Configurator", false, 23)]
        private static void CreateMeshRendererConfigurator()
        {
            var prefab = Resources.Load("Prefabs/MeshRenderer Configurator") as GameObject;
            if (prefab != null)
            {
                var go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                go.name = CreateConfiguratorName(go.name);

                var configurator = go.GetComponent<BaseContainer>();

                SetupConfigurator(configurator);
            }
            else
            {
                Debug.LogError("Configurator prefab not found.");
            }
        }

        [MenuItem("Virtuloc/Configurators/Create Color Configurator", false, 24)]
        private static void CreateColorConfigurator()
        {
            var prefab = Resources.Load("Prefabs/Color Configurator") as GameObject;
            if (prefab != null)
            {
                var go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                go.name = CreateConfiguratorName(go.name);

                var configurator = go.GetComponent<BaseContainer>();

                SetupConfigurator(configurator);
            }
            else
            {
                Debug.LogError("Configurator prefab not found.");
            }
        }
        #endregion

        [MenuItem("Virtuloc/Create Spawnpoint")]
        private static void CreateSpawnpoint()
        {
            var go = new GameObject("Spawnpoint");

            go.transform.localPosition = Vector3.zero;
            go.transform.rotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;

            go.AddComponent<FirstPersonSpawPoint>();


        }

        [MenuItem("Virtuloc/Create Map")]
        private static void CreateMap()
        {
            var mapPrefab = Resources.Load("Prefabs/Map/[Map]") as GameObject;

            var map = PrefabUtility.InstantiatePrefab(mapPrefab); ;
            map.name = "[Map]";
        }

        [MenuItem("Virtuloc/Configurators/Update/Update 2D configurators")]
        private static void Update2DConfigurators()
        {
            var oldConfigurators = GameObject.FindObjectsOfType<Configurator2D>();

            foreach (var oldConfigurator in oldConfigurators)
            {
                //get root transform
                var root = oldConfigurator.transform.parent.transform;

                //get root parent
                var rootParent = root.parent;

                //get root sibling index
                var rootSiblingIndex = root.GetSiblingIndex();

                // create new configurator
                var prefab = Resources.Load("Prefabs/MultimediaConfigurator/[MultimediaConfigurator]") as GameObject;
                var newConfGO = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

                //set root positions and rotations
                newConfGO.transform.position = root.position;
                newConfGO.transform.rotation = root.rotation;
                newConfGO.transform.localScale = root.localScale;

                //set old name
                newConfGO.name = root.name;

                //generate guid for new configurator
                var newConfigurator = newConfGO.GetComponentInChildren<Configurators.MultimediaConfigurator>();
                newConfigurator.GenerateGUID();

                var multiView = newConfigurator.SlideshowSettings.PossibleViews.FirstOrDefault(x => x.gameObject.name == "[MultiView]");

                if (multiView)
                {
                    newConfigurator.SlideshowSettings.PossibleViews.Remove(multiView);
                    GameObject.DestroyImmediate(multiView.gameObject);
                }
                else
                {
                    Debug.LogWarning("[MultiView] not found");
                }

                newConfigurator.SlideshowSettings.PossibleViews.OrderBy(x => x.gameObject.name == "[Single]");

                //set parent to root of new configurator
                if (rootParent != null)
                    newConfGO.transform.parent = rootParent;

                //set sibling index
                newConfGO.transform.SetSiblingIndex(rootSiblingIndex);

                //setConfiguratorAP position and rotation
                newConfigurator.transform.localPosition = oldConfigurator.transform.localPosition;
                newConfigurator.transform.localRotation = oldConfigurator.transform.localRotation;
                newConfigurator.transform.localScale = oldConfigurator.transform.localScale;

                //reassing new configurator to room container
                var roomContainer = GameObject.FindObjectOfType<RoomContainer>();
                var oldIndex = roomContainer.Points.IndexOf(oldConfigurator);
                roomContainer.Points[oldIndex] = newConfigurator;

                //destroy old configurator
                GameObject.DestroyImmediate(root.gameObject);

                Debug.Log($"{newConfGO.name} 2D configurator has been updated to multimedia configurator.", newConfGO);
            }
        }

        private static string CreateConfiguratorName(string name)
        {
            name = name.Replace("(Clone)", "");

            var count = 0;

            foreach (GameObject gameObj in GameObject.FindObjectsOfType<GameObject>())
            {
                if (gameObj.name.StartsWith(name))
                {
                    count++;
                }
            }

            name = count == 1 ? name : $"{name} {count - 1}";

            return name;
        }

        private static void SetupConfigurator(BaseContainer configurator)
        {
            if(configurator == null)
            {
                Debug.LogError("Can't find configurator component");
                return;
            }

            configurator.transform.localPosition = Vector3.zero;
            configurator.transform.rotation = Quaternion.identity;
            configurator.transform.localScale = Vector3.one;

            configurator.GenerateGUID();

            var roomContainer = GameObject.FindObjectOfType<RoomContainer>();

            if (roomContainer == null)
            {
                Debug.LogError("RoomContainer not found!!! Please, preppare virtuloc scene. (Click Virtuloc/Prepare Virtuloc Scene)");
                return;
            }

            var nullPoints = roomContainer.Points.Where(x => x == null).ToList();

            foreach (var item in nullPoints)
            {
                roomContainer.Points.Remove(item);
            }

            roomContainer.Points.Add(configurator as PointConfigurator);

            
        }
    }
}
