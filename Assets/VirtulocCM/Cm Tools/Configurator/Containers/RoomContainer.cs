using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Serialization;
using UnityEngine;

namespace Configurator
{
    public class RoomContainer : BaseContainer
    {
        [SerializeField] private string _name;
        [SerializeField] private List<PointConfigurator> _points;

#if UNITY_EDITOR
        public List<PointConfigurator> Points { get => _points; }

        //private void OnValidate()
        //{
        //    var delete = new List<int>();

        //    for (int i = 0; i < _points.Count; i++)
        //    {
        //        if (_points[i] == null)
        //            delete.Add(i);
        //    }

        //    foreach (var item in delete)
        //    {
        //        _points.RemoveAt(item);
        //    }
        //}
#endif



        #region ISerialize and IDeserialize

        public Data GetDataForSerialization()
        {
            var data = new Data();
            data.Guid = Guid;
            data.Name = _name;
            //data.Points = _points.Select(room => room.GetDataForSerialization()).ToList(_points.Count);
            return data;
        }

        public void LoadDataFromDeserializedJSON(Data data)
        {
            _name = data.Name;

            foreach (PointConfigurator pointConfigurator in _points)
            {
                PointConfigurator.Data pointData = data.Points.Find(d => d.Guid == pointConfigurator.Guid);
                if (pointData != null)
                    pointConfigurator.LoadDataFromDeserializedJSON(pointData);
            }
            
            MarkAsInited();
        }

        public class Data
        {
            public Guid Guid;
            public string Name;
            public List<PointConfigurator.Data> Points;
        }

#endregion
    }
}