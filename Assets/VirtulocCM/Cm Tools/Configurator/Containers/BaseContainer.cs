using System;
using System.Text;
//using Serialization;
using UnityEditor;
using UnityEngine;

namespace Configurator
{
    public abstract class BaseContainer : MonoBehaviour, IConfiguratorIdProvider
    {
        [SerializeField, ReadOnly] private string _guid;

        public event Action OnInitialized;
        public bool IsInitialized { get; private set; }

        public Guid Guid
        {
            get
            {
                if (_guidCache == default)
                    _guidCache = Guid.Parse(_guid);

                return _guidCache;
            }

            private set => _guidCache = value;
        }

        private Guid _guidCache;

        public virtual string ConfiguratorId => Guid.ToString();

#if UNITY_EDITOR
        public void GenerateGUID()
        {
            Guid = Guid.NewGuid();
            _guid = Guid.ToString();
        }
#else
        private void GenerateGUID()
        {
            Guid = Guid.NewGuid();
            _guid = Guid.ToString();
        }
#endif
        protected void MarkAsInited()
        {
            IsInitialized = true;
            OnInitialized?.Invoke();
        }


#if UNITY_EDITOR
        [CustomEditor(typeof(BaseContainer), true)]
        public class BaseConfiguratorEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                if (GUILayout.Button("Generate GUID"))
                {
                    ((BaseContainer) target).GenerateGUID();
                    EditorUtility.SetDirty(target);
                }

                DrawDefaultInspector();
            }
        }
#endif
    }
}