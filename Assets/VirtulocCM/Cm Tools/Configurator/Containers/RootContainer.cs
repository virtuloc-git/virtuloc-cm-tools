using System.Collections.Generic;
using System.Linq;
//using Newtonsoft.Json;
//using Serialization;
using UnityEditor;
using UnityEngine;

namespace Configurator
{
    public class RootContainer : BaseContainer
    {
        [SerializeField] private List<FloorContainer> _floors;
        [SerializeField, TextArea(3, 50)] private string _testJSON;

        [ContextMenu("Try Serialize")]
        public void TrySerialize()
        {
            //string json = JsonConvert.SerializeObject(GetDataForSerialization(), Formatting.Indented);
            //_testJSON = json;
            //Debug.Log(json);
        }
        
        [ContextMenu("Inject JSON")]
        public void InjectJSON()
        {
            //LoadDataFromDeserializedJSON(JsonConvert.DeserializeObject<Data>(_testJSON));
        }


        #region ISerialize and IDeserialize

        public Data GetDataForSerialization()
        {
            var data = new Data();
            //data.Floors = _floors.Select(floor => floor.GetDataForSerialization()).ToList(_floors.Count);
            return data;
        }

        public void LoadDataFromDeserializedJSON(Data data)
        {
            foreach (FloorContainer floorContainer in _floors)
            {
                FloorContainer.Data floorData = data.Floors.Find(d => d.Guid == floorContainer.Guid);
                if (floorData != null)
                    floorContainer.LoadDataFromDeserializedJSON(floorData);
            }
            
            MarkAsInited();
        }

        public class Data
        {
            public List<FloorContainer.Data> Floors;
        }

        #endregion
    }
}