using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Serialization;
using UnityEngine;

namespace Configurator
{
    public class FloorContainer : BaseContainer
    {
        [SerializeField] private List<RoomContainer> _rooms;

        #region ISerialize and IDeserialize

        public Data GetDataForSerialization()
        {
            var data = new Data();
            data.Guid = Guid;
            //data.Rooms = _rooms.Select(room => room.GetDataForSerialization()).ToList(_rooms.Count);
            return data;
        }

        public void LoadDataFromDeserializedJSON(Data data)
        {
            foreach (RoomContainer roomContainer in _rooms)
            {
                RoomContainer.Data roomData = data.Rooms.Find(d => d.Guid == roomContainer.Guid);
                if (roomData != null)
                    roomContainer.LoadDataFromDeserializedJSON(roomData);
            }

            MarkAsInited();
        }

        public class Data
        {
            public Guid Guid;
            public List<RoomContainer.Data> Rooms;
        }

        #endregion
    }
}