using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Configurator
{
    public class ImageConfigurator : PointConfigurator<ImageConfiguratorModel>
    {
        [SerializeField] private List<Image> Targets;
        
        protected override void ApplyConfiguration()
        {
        }
    }
}