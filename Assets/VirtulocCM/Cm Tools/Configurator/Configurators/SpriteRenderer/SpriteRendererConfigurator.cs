using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Configurator
{
    public class SpriteRendererConfigurator : PointConfigurator<SpriteRendererConfiguratorRuntimeData>
    {
        [SerializeField] private SpriteRenderer Target;
        
        protected override void ApplyConfiguration()
        {
        }
    }
}