﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace Configurator
{
    public class AssetBundleGalleryConfigurator : BaseGalleryConfigurator<AssetBundleGalleryConfiguratorModel>
    {
        [SerializeField] private List<Transform> Targets;

        protected override void ApplyConfiguration()
        {
        }
    }
}
