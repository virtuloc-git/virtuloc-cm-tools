﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Configurator
{
    public abstract class BaseGalleryConfigurator<TSerializeData> : PointConfigurator<TSerializeData> where TSerializeData : new()
    {
    }
}