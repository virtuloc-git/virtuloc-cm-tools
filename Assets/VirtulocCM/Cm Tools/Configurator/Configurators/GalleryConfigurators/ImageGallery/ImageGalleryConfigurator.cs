﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Configurator
{
    public class ImageGalleryConfigurator : BaseGalleryConfigurator<ImageGalleryConfiguratorModel>
    {
        [SerializeField] private List<Image> Targets;

        protected override void ApplyConfiguration()
        {
        }
    }
}
