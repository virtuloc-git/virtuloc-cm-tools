﻿using UnityEngine;
using UnityEngine.UI;

namespace Configurator.Map
{
    public class MapTargetModel : MonoBehaviour
    {
        [SerializeField]
        private MapPointConfigurator m_Configurator;
        [SerializeField]
        private Button m_Button;

        public Button Button { get => m_Button; set => m_Button = value; }
    }
}
