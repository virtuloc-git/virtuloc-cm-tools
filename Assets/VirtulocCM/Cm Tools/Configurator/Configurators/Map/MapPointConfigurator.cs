﻿using UnityEngine;
using Virtuloc.Utils;

namespace Configurator.Map
{
    public class MapPointConfigurator : PointConfigurator<MapPointModel>
    {
        protected override void ApplyConfiguration()
        {

        }

#if UNITY_EDITOR

        protected override Color32 m_GizmoColor => new Color32(246,229,21,127);        
#endif

    }
}
