﻿using System.Collections.Generic;
using UnityEngine;

namespace Configurator.Map
{
    public class MapController : MonoBehaviour
    {
        [SerializeField]
        private List<MapTargetModel> m_Targets;

        [SerializeField]
        private Color m_ActiveColor = new Color(21f/ 255f, 84f / 255f, 246f / 255f);

        [SerializeField]
        private Color m_DisabledColor = new Color(123f / 255f, 123f / 255f, 123f / 255f);

        [SerializeField]
        private Color m_TextColor = new Color(50f / 255f, 50f / 255f, 50f / 255f);

        public List<MapTargetModel> Targets { get => m_Targets;}

#if UNITY_EDITOR
        private void OnValidate()
        {
            var delete = new List<int>();

            for (int i = 0; i < m_Targets.Count; i++)
            {
                if (m_Targets[i] == null)
                    delete.Add(i);
            }

            foreach (var item in delete)
            {
                m_Targets.RemoveAt(item);
            }
        }
#endif

    }
}
