using System;
using System.Collections.Generic;
//using Newtonsoft.Json;
//using Serialization;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Configurator
{
    public abstract class PointConfigurator<TSerializeData> : PointConfigurator where TSerializeData : new()
    {
        protected TSerializeData Data = new TSerializeData();
        
        protected abstract void ApplyConfiguration();

        protected override string SerializeData()
        {
            return "";
        }

        protected override void DeserializeData(string json)
        {
            //Data = JsonConvert.DeserializeObject<TSerializeData>(json);
            //ApplyConfiguration();
        }
    }

    public abstract class PointConfigurator : BaseContainer
    {
        protected abstract string SerializeData();
        protected abstract void DeserializeData(string json);

        #region ISerialize and IDeserialize

        public Data GetDataForSerialization()
        {
            var data = new Data();
            data.Guid = Guid;
            data.PointData = SerializeData();
            return data;
        }

        public void LoadDataFromDeserializedJSON(Data data)
        {
            DeserializeData(data.PointData);
            
            MarkAsInited();
        }

        public class Data
        {
            public Guid Guid;
            public string PointData;
        }

        #endregion

#if UNITY_EDITOR
        protected virtual Color32 m_GizmoColor => new Color32(21, 93, 246, 127);

        protected virtual void OnDrawGizmos()
        {
            Gizmos.color = m_GizmoColor;
            Gizmos.DrawSphere(transform.position, .5f);
        }

#endif
    }
}