﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;


namespace Configurator
{
    public class Configurator3D : PointConfigurator<Configurator3DModel>
    {
        [SerializeField]
        private Transform m_Target;

        public Transform Target { get => m_Target; }

        protected override void ApplyConfiguration()
        {
            
        }
    }
}
