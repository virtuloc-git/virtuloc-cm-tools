﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Configurator
{
    public class Configurator3DModel
    {
        public int Id { get; set; }
        public Metadata3D Metadata { get; set; }

        #region [TEMP]
        public string TempLocalUrl { get; set; }
        #endregion

    }

    public class Metadata3D
    {
        public Vector3 Position { get; set; }
        public Vector3 Scale { get; set; }
        public Quaternion Rotation { get; set; }

        public Metadata3D()
        {
            Position = Vector3.zero;
            Rotation = Quaternion.identity;
            Scale = Vector3.one;
        }
    }

}