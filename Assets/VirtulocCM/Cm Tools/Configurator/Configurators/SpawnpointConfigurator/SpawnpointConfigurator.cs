﻿using UnityEngine;

namespace Configurator
{
    public class SpawnpointConfigurator : PointConfigurator<SpawnpointConfiguratorModel>
    {
        protected override void ApplyConfiguration()
        {
           
        }

#if UNITY_EDITOR

        protected override void OnDrawGizmos()
        {
            
        }

#endif
    }

    public class SpawnpointConfiguratorModel
    {

    }
}
