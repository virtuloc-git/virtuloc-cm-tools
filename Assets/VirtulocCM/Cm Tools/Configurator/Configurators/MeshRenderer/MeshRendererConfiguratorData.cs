using System;
using System.Collections.Generic;
using UnityEngine;

namespace Configurator
{
    [Serializable]
    public class MeshRendererConfiguratorData : BaseConfiguratorData
    {
        public List<Material> Materials;
    }
}