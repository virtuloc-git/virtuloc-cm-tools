using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Configurator
{
    public class MeshRendererConfigurator : PointConfigurator<MeshRendererConfiguratorModel>
    {
        [SerializeField] private List<MeshRenderer> Targets;
        [SerializeField] private List<MeshRendererConfiguratorData> Presets;
        
        protected MeshRendererConfiguratorData CurrentPreset => Presets.Find(p => p.ID == Data.SelectedPresetID);
        
        protected override void ApplyConfiguration()
        {
            var currentPreset = CurrentPreset;
            for (int i = 0; i < Targets.Count; i++)
            {
                var target = Targets[i];
                var targetMaterials = target.materials;
                var presetMaterials = currentPreset.Materials;
                int max = Mathf.Min(targetMaterials.Length, presetMaterials.Count);
                for (int j = 0; j < max; j++)
                {
                    targetMaterials[j].CopyPropertiesFromMaterial(presetMaterials[j]);
                }
            }
            
            
        }
    }
}