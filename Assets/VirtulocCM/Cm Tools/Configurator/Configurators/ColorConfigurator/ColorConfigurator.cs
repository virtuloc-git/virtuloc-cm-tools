using System.Collections.Generic;
using UnityEngine;

namespace Configurator
{
    public class ColorConfigurator : PointConfigurator<ColorConfiguratorModel>
    {
        [SerializeField] private List<MeshRenderer> Targets;
        [SerializeField] private List<int> MaterialIndexesToChange;

        private static readonly int Color = Shader.PropertyToID("_Color");
        private MaterialPropertyBlock _materialProperty;
        private MaterialPropertyBlock MaterialProperty => _materialProperty ?? (_materialProperty = new MaterialPropertyBlock());

        protected override void ApplyConfiguration()
        {
            UpdateColor();
        }

        private void UpdateColor()
        {
            MaterialProperty.SetColor(Color, Data.Color);

            if (MaterialIndexesToChange.Count == 0)
            {
                for (int i = 0; i < Targets.Count; i++)
                {
                    Targets[i].SetPropertyBlock(MaterialProperty);
                }
            }
            else
            {
                for (int i = 0; i < Targets.Count; i++)
                {
                    var target = Targets[i];
                    for (int materialIndex = 0; materialIndex < target.materials.Length; materialIndex++)
                    {
                        if (MaterialIndexesToChange.Contains(materialIndex))
                            target.SetPropertyBlock(MaterialProperty, materialIndex);
                    }
                }
            }
        }
    }
}