﻿using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerBehaviour : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer m_VideoPlayer;

    [SerializeField]
    private MeshRenderer m_VideoRenderer;


    public void SetURL(string url)
    {
        if (!url.Equals(""))
        {
            m_VideoPlayer.url = url;
        }
    }

    public void Stop()
    { 
        m_VideoPlayer.Pause();
        m_VideoPlayer.time = 0f;
    }

    public void Play()
    {
        m_VideoPlayer.Play();
    }

    public void Pause()
    {
        m_VideoPlayer.Pause();
    }

    void OnDisable()
    {
        Stop();
    }
}
