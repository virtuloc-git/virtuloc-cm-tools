﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Configurator
{
    [Obsolete("Use Multimedia Configurator instead.")]
    public class Configurator2D : PointConfigurator<Configurator2DModel>
    {
        [SerializeField] private List<Image> Targets;

        protected override void ApplyConfiguration()
        {
            
        }
    }

    public enum ConfiguratorType2D
    {
        IMAGE,
        VIDEO
    }
}
