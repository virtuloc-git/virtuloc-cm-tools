﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configurator
{
    public class Configurator2DModel
    {
        public int MediaId { get; set; }
        public string Url { get; set; }
        public ConfiguratorType2D Type { get; set; }
    }
}
