﻿namespace Virtuloc.Event
{
    public class LiveStreamZoneConfiguratorModel
    {
        public string YoutubeURL { get; set; }

        public string GetStreamIdFromUrl()
        {
            if (string.IsNullOrEmpty(YoutubeURL))
                return string.Empty;

            YoutubeURL = YoutubeURL.Replace("https://", "");

            if(YoutubeURL.StartsWith("www.youtube.com/watch?v="))
            {
                return YoutubeURL.Replace("www.youtube.com/watch?v=", "");
            }

            return string.Empty;
        }
    }
}
