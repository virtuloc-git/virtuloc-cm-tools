﻿using System;
using UnityEngine;

namespace Virtuloc.Event
{
    [RequireComponent(typeof(BoxCollider))]
    public class LiveStreamZoneTrigger : MonoBehaviour
    {
        public event Action<Collider> OnTriggerEntered;
        public event Action<Collider> OnTriggerExited;

        private void OnTriggerEnter(Collider other)
        {
            OnTriggerEntered?.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            OnTriggerExited?.Invoke(other);
        }
    }
}
