﻿using Configurator;
using UnityEngine;

namespace Virtuloc.Event
{
    public class LiveStreamZoneConfigurator : PointConfigurator<LiveStreamZoneConfiguratorModel>
    {
        [SerializeField]
        private LiveStreamZoneTrigger _trigger;

        protected override void ApplyConfiguration()
        {
            
        }       
    }
}
