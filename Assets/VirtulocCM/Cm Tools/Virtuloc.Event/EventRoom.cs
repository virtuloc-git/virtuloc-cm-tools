﻿using UnityEngine;
using System;
using System.Collections.Generic;
using TMPro;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Virtuloc.Event
{
    [RequireComponent(typeof(BoxCollider))]
    public class EventRoom : MonoBehaviour
    {
        [SerializeField]
        private string _password;

        [SerializeField]
        private string _roomId;

        [SerializeField]
        private int _maxUsersInCall = 20;

        [Header("User counters")]
        [SerializeField]
        private List<TMP_Text> _counters;

        [SerializeField]
        private string _format = "{actual} / {max}";

        private BoxCollider _boxCollider;

        private void Awake()
        {
            _boxCollider = GetComponent<BoxCollider>();
        }

        internal void GenerateRoomID()
        {
            _roomId = Guid.NewGuid().ToString();
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.tag == "Player")
            {
                Debug.Log($"[{nameof(EventRoom)}]: Start call. Room ID: {_roomId}");
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                Debug.Log($"[{nameof(EventRoom)}]: End call. Room ID: {_roomId}");
            }
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(EventRoom), true)]
    public class EventRoomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Generate Room ID"))
            {
                ((EventRoom)target).GenerateRoomID();
                EditorUtility.SetDirty(target);
            }

            DrawDefaultInspector();
        }
    }
#endif
}
