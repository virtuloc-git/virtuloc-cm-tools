﻿using System;
using System.Runtime.CompilerServices;

namespace Virtuloc.Networking
{
    public class PersistantDataUpdatedEventArgs : EventArgs
    {
        public string Key;
        public string Data;

        public PersistantDataUpdatedEventArgs()
        {

        }

        public PersistantDataUpdatedEventArgs(string key, string data)
        {
            Key = key;
            Data = data;
        }
    }
}