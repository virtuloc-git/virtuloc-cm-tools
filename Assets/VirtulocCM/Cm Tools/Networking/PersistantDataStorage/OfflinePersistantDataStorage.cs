﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Virtuloc.Networking
{
    public class OfflinePersistantDataStorage : IPersistantDataStorage
    {
        public event EventHandler<PersistantDataUpdatedEventArgs> OnPersistantDataUpdated;
        public event EventHandler<PersistantDataRecievedEventArgs> OnDataRecieved;

        private Dictionary<string, string> _data = new Dictionary<string, string>();

        public OfflinePersistantDataStorage()
        {
            
        }

        public void GetData(string key, DataScope scope)
        {
            var dict = new Dictionary<string, string>();

            foreach (var dataKey in _data.Keys.Where(x => x == key || x.StartsWith($"{key}.")))
            {
                dict[dataKey] = _data[dataKey];
            }

            OnDataRecieved?.Invoke(this, new PersistantDataRecievedEventArgs(dict));
        }

        public void SetData(string key, string data, DataScope scope)
        {
            _data[key] = data;

            OnPersistantDataUpdated?.Invoke(this, new PersistantDataUpdatedEventArgs()
            {
                Key = key,
                Data = data
            });
        }
    }
}
