﻿using System;

namespace Virtuloc.Networking
{
    public interface IPersistantDataStorage
    {
        event EventHandler<PersistantDataUpdatedEventArgs> OnPersistantDataUpdated;
        event EventHandler<PersistantDataRecievedEventArgs> OnDataRecieved;

        void GetData(string key, DataScope scope);
        void SetData(string key, string data, DataScope scope);
    }
}
