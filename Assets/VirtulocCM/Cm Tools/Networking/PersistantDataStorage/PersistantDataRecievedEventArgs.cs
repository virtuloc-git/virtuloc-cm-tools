﻿using System;
using System.Collections.Generic;

namespace Virtuloc.Networking
{
    public class PersistantDataRecievedEventArgs : EventArgs
    {
        public Dictionary<string, string> Data { get; }

        public PersistantDataRecievedEventArgs()
        {
            Data = new Dictionary<string, string>();
        }

        public PersistantDataRecievedEventArgs(Dictionary<string, string> data)
        {
            Data = data;
        }
    }
}