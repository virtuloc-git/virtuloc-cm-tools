using System;
using System.Collections.Concurrent;
using UnityEngine;
//using Virtuloc.Showroom.Controllers;

namespace Virtuloc.Networking
{
    public class PersistantDataStorageBehaviour : MonoBehaviour
    {
        IPersistantDataStorage _persistantDataStorage;

        private ConcurrentDictionary<string, ConcurrentBag<Action<string>>> _requests;

        public event Action<PersistantDataUpdatedEventArgs> OnDataUpdated;

        // Start is called before the first frame update
        void Awake()
        {
            _requests = new ConcurrentDictionary<string, ConcurrentBag<Action<string>>>();

            //if (ApplicationController.Instance.Mode.Value == Virtuloc.Showroom.Enums.ApplicationMode.EVENT)
            //{
            //    _persistantDataStorage = DarkRiftPersistantDataStorage.Instance;
            //}
            //else
            //{
            _persistantDataStorage = new OfflinePersistantDataStorage();
            //}

            _persistantDataStorage.OnDataRecieved += Handle_persistantDataStorage_OnDataRecieved;
            _persistantDataStorage.OnPersistantDataUpdated += Handle_persistantDataStorage_OnDataUpdated;
        }

        private void Handle_persistantDataStorage_OnDataUpdated(object sender, PersistantDataUpdatedEventArgs e)
        {
            OnDataUpdated?.Invoke(e);
        }

        private void Handle_persistantDataStorage_OnDataRecieved(object sender, PersistantDataRecievedEventArgs e)
        {
            foreach (var item in e.Data)
            {
                if (_requests.ContainsKey(item.Key))
                {
                    if(e.Data.Count > 0)
                    {
                        foreach (var callback in _requests[item.Key])
                        {
                            callback?.Invoke(item.Value);
                        }
                    }

                    _ = _requests.TryRemove(item.Key, out _);
                }
            }
        }

        public void GetDataEvent(PlayMakerFSM reciever, string variable, string key, string eventName, DataScope scope)
        {
            RegisterRequestEvent(key, reciever, variable, eventName);

            _persistantDataStorage.GetData(key, scope);
        }

        public void GetDataState(PlayMakerFSM reciever, string variableName, string key, string state, DataScope scope)
        {
            RegisterRequestState(key, reciever, variableName, state);

            _persistantDataStorage.GetData(key, scope);
        }
        private void RegisterRequestEvent(string key, PlayMakerFSM reciever, string variableName, string eventName)
        {
            if (!_requests.ContainsKey(key))
                _requests[key] = new ConcurrentBag<Action<string>>();

            _requests[key].Add((data) =>
            {
                var variable = reciever.FsmVariables.FindFsmString(variableName);

                if(variable != null)
                {
                    variable.Value = data;
                    reciever.SendEvent(eventName);
                }
            });
        }

        private void RegisterRequestState(string key, PlayMakerFSM reciever, string variableName, string stateName)
        {
            if (!_requests.ContainsKey(key))
                _requests[key] = new ConcurrentBag<Action<string>>();

            _requests[key].Add((data) =>
            {
                var variable = reciever.FsmVariables.FindFsmString(variableName);

                if (variable != null)
                {
                    variable.Value = data;
                    reciever.SetState(stateName);
                }
            });
        }

        public void SetData(string key, string data, DataScope scope)
        {
            _persistantDataStorage.SetData(key, data, scope);
        }

        // void Update(){
        //     if(Input.GetKeyDown(KeyCode.T))
        //         SetData("test_key", "test_data", DataScope.ROOM_INSTANCE);

        //     if(Input.GetKeyDown(KeyCode.R))
        //         SetData("test_key2", "test_data2", DataScope.ROOM_INSTANCE);
        // }

        void OnDestroy(){
            if(_persistantDataStorage != null)
            {
                _persistantDataStorage.OnDataRecieved -= Handle_persistantDataStorage_OnDataRecieved;
                _persistantDataStorage.OnPersistantDataUpdated -= Handle_persistantDataStorage_OnDataUpdated;
            }
        }
    }
}
