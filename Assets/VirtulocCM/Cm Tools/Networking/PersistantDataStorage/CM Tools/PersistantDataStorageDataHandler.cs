using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Virtuloc.Networking{
    public class PersistantDataStorageDataHandler : MonoBehaviour
    {
        [SerializeField] private PersistantDataStorageBehaviour _storage;
        [SerializeField] private List<RecieverData> _recievers;

        [System.Serializable]
        public class RecieverData
        {
            public PlayMakerFSM Reciever; 
            public string Variable; 
            public string Key; 
            /// <summary>
            ///Name of playmaker event or playmaker state, based on <see cref="RecieverType"/>
            /// </summary>
            [Tooltip("Name of playmaker event or playmaker state, based on Reciever Type")]
            public string EndpointName;
            public RecieverType RecieverType;
        }

        public enum RecieverType{
            STATE = 0,
            EVENT = 1
        }

        private void Awake()
        {
            if(_storage != null)
                _storage.OnDataUpdated += Handle_OnDataUpdated;
        }

        private void Handle_OnDataUpdated(PersistantDataUpdatedEventArgs obj)
        {
            var rData = _recievers.Where(x => x.Key == obj.Key);

            if(rData == null || rData.Count() == 0)
                return;

            foreach(var recieverData in rData){
                var variable = recieverData.Reciever.FsmVariables.FindFsmString(recieverData.Variable);

                if(variable != null)
                {
                    variable.Value = obj.Data;
                    if(recieverData.RecieverType == RecieverType.EVENT)
                    {
                        recieverData.Reciever.SendEvent(recieverData.EndpointName);
                    }
                    else if(recieverData.RecieverType == RecieverType.STATE)
                    {
                        recieverData.Reciever.SetState(recieverData.EndpointName);
                    }
                }
            }
        }

        public void  RegisterReciever(PlayMakerFSM reciever, string variable, string key, string endpointName, RecieverType recieverType)
        {
            _recievers?.Add(new RecieverData(){
                Reciever = reciever,
                Variable = variable,
                Key = key,
                EndpointName = endpointName,
                RecieverType = recieverType
            });
        }

        public void UnregisterReciever(PlayMakerFSM reciever)
        {
            var affectedRecievers = _recievers?.Where(x => x.Reciever == reciever).ToList();

            if(affectedRecievers?.Count() > 0)
            {
                foreach(var r in affectedRecievers)
                {
                    _recievers.Remove(r);
                }
            }
        }

        private void OnDestroy()
        {
            if(_storage != null)
                _storage.OnDataUpdated -= Handle_OnDataUpdated;
        }
    }
}