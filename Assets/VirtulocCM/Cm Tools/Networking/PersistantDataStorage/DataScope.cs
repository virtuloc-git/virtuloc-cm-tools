﻿namespace Virtuloc.Networking
{
    public enum DataScope
    {
        SHOWROOM = 0,
        ROOM = 1,
        ROOM_INSTANCE = 2
    }
}
