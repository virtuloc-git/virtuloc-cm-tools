﻿using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Virtuloc.Networking.Shared;

namespace Virtuloc.Networking
{
    [RequireComponent(typeof(PhotonView))]
    public class BaseSynchronzableDataStorage : MonoBehaviourPunCallbacks
    {
        //[SerializeField]
        //private ISynchronizableDataObject m_Target;

        //public ISynchronizableDataObject Target { get => m_Target; }

        //private PhotonView m_PhotonView;

        //private void Awake()
        //{
        //    //if (ApplicationController.Instance.Mode.Value != Showroom.Enums.ApplicationMode.PLAYER)
        //    //    Destroy(this);
        //}


        //void Start()
        //{
        //    photonView.OwnershipTransfer = OwnershipOption.Takeover;

        //    if (Target == null)
        //    {
        //        m_Target = GetComponent<ISynchronizableDataObject>();
        //    }

        //    if (Target == null)
        //    {
        //        Debug.LogError("MISSING TAGET!!!", gameObject);
        //        //throw new System.Exception("Missing target for synchronizable object.");
        //        return;
        //    }

        //    if (PhotonNetwork.OfflineMode)
        //    {
        //        Target.OnDataAdded += Target_OnDataAdded;
        //        Target.OnDataChanged += Target_OnDataChanged;
        //        Target.OnDataDeleted += Target_OnDataDeleted;

        //        Target.OnSynchronizationRequested += Target_OnSynchronizationRequested;
        //    }

        //    //if (!photonView.IsMine && PhotonNetwork.InRoom)
        //    //{
        //    //    photonView.RPC("RequestSynchronizeData", RpcTarget.All);
        //    //}

        //    HandleOwnership();
        //}

        //private void Target_OnDataDeleted(string key)
        //{
        //    if (Target == null)
        //    {
        //        m_Target = GetComponent<ISynchronizableDataObject>();
        //    }

        //    if (Target == null)
        //    {
        //        Debug.LogError("MISSING TAGET!!!", gameObject);
        //    }

        //    if (NetworkingBehaviour.Instance.UserRole == UserRole.MASTER)
        //    {
        //        photonView.RPC("Master_DeleteData", RpcTarget.All, key);
        //    }
        //    else
        //    {
        //        photonView.RPC("Client_DeleteData", RpcTarget.MasterClient, key);
        //    }
        //}

        //private void Target_OnDataChanged(Tuple<string, string> data)
        //{
        //    var dataArray = new string[2] { data.Item1, data.Item2 };

        //    if (Target == null)
        //    {
        //        m_Target = GetComponent<ISynchronizableDataObject>();
        //    }

        //    if (Target == null)
        //    {
        //        Debug.LogError("MISSING TAGET!!!", gameObject);
        //    }

        //    if (NetworkingBehaviour.Instance.UserRole == UserRole.MASTER)
        //    {
        //        photonView.RPC("Master_ChangeData", RpcTarget.All, dataArray);
        //    }
        //    else
        //    {
        //        photonView.RPC("Client_ChangeData", RpcTarget.MasterClient, dataArray);
        //    }
        //}

        //private void Target_OnDataAdded(Tuple<string, string> data)
        //{
        //    var dataArray = new string[2] { data.Item1, data.Item2 };

        //    if (Target == null)
        //    {
        //        m_Target = GetComponent<ISynchronizableDataObject>();
        //    }

        //    if (Target == null)
        //    {
        //        Debug.LogError("MISSING TAGET!!!", gameObject);
        //    }

        //    if (NetworkingBehaviour.Instance.UserRole == UserRole.MASTER)
        //    {
        //        photonView.RPC("Master_AddData", RpcTarget.All, dataArray);
        //    }
        //    else
        //    {
        //        photonView.RPC("Client_AddData", RpcTarget.MasterClient, dataArray);
        //    }
        //}

        //private void HandleOwnership()
        //{
        //    if (NetworkingBehaviour.Instance.UserRole == UserRole.MASTER)
        //    {
        //        if (!photonView.IsMine)
        //        {
        //            photonView.TransferOwnership(PhotonNetwork.LocalPlayer);

        //        }
        //    }
        //}

        //[PunRPC]
        //public void Master_ChangeData(string[] data)
        //{
        //    if (Target == null || data == null)
        //        return;

        //    Target.ChangeData(Tuple.Create(data[0], data[1]));
        //}

        //[PunRPC]
        //public void Master_AddData(string[] data)
        //{
        //    if (Target == null || data == null)
        //        return;

        //    Target.AddData(Tuple.Create(data[0], data[1]));
        //}

        //[PunRPC]
        //public void Master_DeleteData(string key)
        //{
        //    if (Target == null || string.IsNullOrEmpty(key))
        //        return;

        //    Target.DeleteData(key);
        //}

        //public void Master_ApplyStringData(Dictionary<string, string> data)
        //{
        //    if (Target == null || data == null)
        //        return;

        //    Target.MasterApplyData(new SynchronizableStringData(data));
        //}

        //[PunRPC]
        //public void Client_ChangeData(string[] data)
        //{
        //    if (Target == null || data == null)
        //        return;

        //    Target.ChangeData(Tuple.Create(data[0], data[1]));

        //    photonView.RPC("Master_ChangeData", RpcTarget.Others, data);
        //}

        //[PunRPC]
        //public void Client_AddData(string[] data)
        //{
        //    if (Target == null || data == null)
        //        return;

        //    Target.AddData(Tuple.Create(data[0], data[1]));

        //    photonView.RPC("Master_AddData", RpcTarget.Others, data);
        //}

        //[PunRPC]
        //public void Client_DeleteData(string key)
        //{
        //    if (Target == null || string.IsNullOrEmpty(key))
        //        return;

        //    Target.DeleteData(key);

        //    photonView.RPC("Master_DeleteData", RpcTarget.Others, key);
        //}

        //[PunRPC]
        //public void RequestSynchronizeStringData()
        //{
        //    if (NetworkingBehaviour.Instance.UserRole == UserRole.MASTER)
        //    {
        //        photonView.RPC("Master_ApplyStringData", RpcTarget.Others, Target.GetSynchronizedData()?.Data);
        //    }
        //}

        //public override void OnJoinedRoom()
        //{
        //    base.OnJoinedRoom();

        //    HandleOwnership();

        //    if (Target == null)
        //    {
        //        m_Target = GetComponent<ISynchronizableDataObject>();
        //    }

        //    if (Target != null)
        //    {
        //        Target.OnDataAdded += Target_OnDataAdded;
        //        Target.OnDataChanged += Target_OnDataChanged;
        //        Target.OnDataDeleted += Target_OnDataDeleted;

        //        Target.OnSynchronizationRequested += Target_OnSynchronizationRequested;


        //        if (NetworkingBehaviour.Instance.UserRole != UserRole.MASTER)
        //        {
        //            photonView.RPC("RequestSynchronizeStringData", RpcTarget.MasterClient);
        //        }
        //    }
        //    else
        //    {
        //        Debug.LogError($"Target is null {gameObject.name}", gameObject);
        //    }
        //}

        //private void Target_OnSynchronizationRequested()
        //{
        //    if (NetworkingBehaviour.Instance.UserRole != UserRole.MASTER)
        //    {
        //        photonView.RPC("RequestSynchronizeStringData", RpcTarget.MasterClient);
        //    }
        //}

        //private void OnDestroy()
        //{
        //    if (Target != null)
        //    {
        //        Target.OnDataAdded -= Target_OnDataAdded;
        //        Target.OnDataChanged -= Target_OnDataChanged;
        //        Target.OnDataDeleted -= Target_OnDataDeleted;

        //        Target.OnSynchronizationRequested -= Target_OnSynchronizationRequested;
        //    }
        //}
    }
}
