﻿using UnityEngine;
using UnityEngine.Events;

namespace Virtuloc.Networking
{
    public class KeyEventComparer : MonoBehaviour
    {
        [SerializeField]
        private string _key;

        public UnityEvent OnValueMatch;
        public UnityEvent OnValueNotMatch;

        public void EventInvoked(string key, string value)
        {
            if (_key == key)
            {
                OnValueMatch?.Invoke();
            }
            else
            {
                OnValueNotMatch?.Invoke();
            }
        }
    }

}
