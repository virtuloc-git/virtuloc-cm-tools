﻿using UnityEngine;
using UnityEngine.Events;

namespace Virtuloc.Networking
{
    public class KeyValueEventComparer : MonoBehaviour
    {
        [SerializeField]
        private string _key;
        [SerializeField]
        private string _value;

        public UnityEvent OnKeyMatch;
        public UnityEvent OnKetNotMatch;
        public UnityEvent OnValueMatch;
        public UnityEvent OnValueNotMatch;

        public void EventInvoked(string key, string value)
        {
            if (_key == key)
            {
                OnKeyMatch?.Invoke();

                if (_value == value)
                {
                    OnValueMatch?.Invoke();
                }
                else
                {
                    OnValueNotMatch?.Invoke();
                }
            }
            else
            {
                OnKetNotMatch?.Invoke();
            }
        }

    }

}
