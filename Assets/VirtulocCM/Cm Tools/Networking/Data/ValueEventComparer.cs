﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Virtuloc.Networking
{
    public class ValueEventComparer : MonoBehaviour
    {
        [SerializeField]
        private string _value;

        public UnityEvent OnKeyMatch;
        public UnityEvent OnKeyNotMatch;

        public void EventInvoked(string key, string value)
        {
            if (_value == key)
            {
                OnKeyMatch?.Invoke();
            }
            else
            {
                OnKeyNotMatch?.Invoke();
            }
        }
    }
}
