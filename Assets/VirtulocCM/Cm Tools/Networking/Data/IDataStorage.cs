﻿using System;

namespace Virtuloc.Networking
{
    internal interface IDataStorage
    {
        event Action<Tuple<string, string>> OnDataChanged;
        event Action<Tuple<string, string>> OnDataAdded;
        event Action<string> OnDataDeleted;

        /// <summary>
        /// Is key presented in dictionary?
        /// </summary>
        /// <param name="key">Key Identifier</param>
        /// <returns>True id key is presented in dictionary, else False.</returns>
        bool ContainsKey(string key);
        /// <summary>
        /// Delete data stored at key.
        /// </summary>
        /// <param name="key">Key Identifier</param>
        void DeleteData(string key);
        /// <summary>
        /// Get stored data at key.
        /// </summary>
        /// <param name="key">Key Identifier</param>
        /// <returns>Stored data if key is presented in dictionary, else empty string.</returns>
        string GetData(string key);
        /// <summary>
        /// Store or update data.
        /// </summary>
        /// <param name="key">Key Identifier</param>
        /// <param name="value"></param>
        void StoreData(string key, string value);
    }
}