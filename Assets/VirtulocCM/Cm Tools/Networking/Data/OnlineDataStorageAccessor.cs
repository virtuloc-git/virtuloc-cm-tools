﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Virtuloc.Networking
{
    public class OnlineDataStorageAccessor : MonoBehaviour, IDataStorage
    {
        private IDataStorage _dataStorage;

        public event Action OnOnlineDataChanged;
        public event Action<Tuple<string, string>> OnDataChanged;
        public event Action<Tuple<string, string>> OnDataAdded;
        public event Action<string> OnDataDeleted;

        public TupleStringUnityEvent OnDataAdded_UE;
        public TupleStringUnityEvent OnDataChanged_UE;
        public StringUnityEvent OnDataDeleted_UE;


        private void Awake()
        {
            _dataStorage = GameObject.FindObjectOfType<OnlineDataStorage>();

            if(_dataStorage == null)
            {
                _dataStorage = new GameObject("OnlineDataStorage").AddComponent<OnlineDataStorage>();
            }

            _dataStorage.OnDataChanged += HandleOnDataChanged;
            _dataStorage.OnDataAdded += HandleOnDataAdded;
            _dataStorage.OnDataDeleted += HandleOnDataDeleted;
        }

        private void HandleOnDataDeleted(string key)
        {
            OnDataDeleted?.Invoke(key);
            OnDataDeleted_UE?.Invoke(key);
        }

        private void HandleOnDataAdded(Tuple<string, string> data)
        {
            OnDataAdded?.Invoke(data);
            OnDataAdded_UE?.Invoke(data.Item1, data.Item2);
        }

        private void HandleOnDataChanged(Tuple<string, string> data)
        {
            OnDataChanged?.Invoke(data);
            OnDataChanged_UE?.Invoke(data.Item1, data.Item2);
        }

        #region [IDataStorage Implementation]
        public bool ContainsKey(string key)
        {
            return _dataStorage.ContainsKey(key);
        }

        public void DeleteData(string key)
        {
            _dataStorage?.DeleteData(key);
        }

        public string GetData(string key)
        {
            return _dataStorage.GetData(key);
        }

        public void StoreData(string key, string value)
        {
            _dataStorage.StoreData(key, value);
        }
        #endregion

        private void OnDestroy()
        {
            if (_dataStorage != null)
            {
                _dataStorage.OnDataChanged -= HandleOnDataChanged;
                _dataStorage.OnDataAdded -= HandleOnDataAdded;
                _dataStorage.OnDataDeleted -= HandleOnDataDeleted;
            }
        }

        public void LogAddedTuple(string i1, string i2)
        {
            Debug.Log($"Added {i1}: {i2}");
        }

        public void LogChangedTuple(string i1, string i2)
        {
            Debug.Log($"Changed {i1}: {i2}");
        }
    }

    [System.Serializable]
    public class TupleStringUnityEvent : UnityEvent<string, string>
    {

    }

    [System.Serializable]
    public class StringUnityEvent : UnityEvent<string>
    {

    }

}
