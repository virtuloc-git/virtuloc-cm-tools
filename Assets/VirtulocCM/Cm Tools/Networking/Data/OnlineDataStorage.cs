﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Virtuloc.Networking.Shared;

namespace Virtuloc.Networking
{
    public class OnlineDataStorage : MonoBehaviour, ISynchronizableDataObject, IDataStorage
    {
        public event Action OnSynchronizationRequested;

        public event Action<Tuple<string, string>> OnDataChanged;
        public event Action<Tuple<string, string>> OnDataAdded;
        public event Action<string> OnDataDeleted;

        /// <summary>
        /// Stored on-line data.
        /// Key: Key Identifier
        /// Value: stored data as string
        /// </summary>
        private Dictionary<string, string> m_Data { get; set; }

        private void Awake()
        {
            m_Data = new Dictionary<string, string>();
        }

        #region [IDataStorage Implementation]
        /// <summary>
        /// Store or update data.
        /// </summary>
        /// <param name="key">Key Identifier</param>
        /// <param name="value"></param>
        public void StoreData(string key, string value)
        {
            if (m_Data == null)
                m_Data = new Dictionary<string, string>();

            if (!m_Data.ContainsKey(key))
            {
                m_Data.Add(key, value);

                OnDataAdded?.Invoke(Tuple.Create(key, value));
            }
            else
            {
                m_Data[key] = value;

                OnDataChanged?.Invoke(Tuple.Create(key, value));
            }
        }

        /// <summary>
        /// Delete data stored at key.
        /// </summary>
        /// <param name="key">Key Identifier</param>
        public void DeleteData(string key)
        {
            if (m_Data.ContainsKey(key))
                m_Data.Remove(key);

            OnDataDeleted?.Invoke(key);
        }

        /// <summary>
        /// Get stored data at key.
        /// </summary>
        /// <param name="key">Key Identifier</param>
        /// <returns>Stored data if key is presented in dictionary, else empty string.</returns>
        public string GetData(string key)
        {
            if (!(m_Data?.ContainsKey(key) ?? false))
                return string.Empty;

            return m_Data[key];
        }

        /// <summary>
        /// Is key presented in dictionary?
        /// </summary>
        /// <param name="key">Key Identifier</param>
        /// <returns>True id key is presented in dictionary, else False.</returns>
        public bool ContainsKey(string key)
        {
            return m_Data?.ContainsKey(key) ?? false;
        }
        #endregion

        public SynchronizableStringData GetSynchronizedData()
        {
            return new SynchronizableStringData(m_Data);
        }

        public void ChangeData(Tuple<string, string> data)
        {
            if (m_Data == null)
                m_Data = new Dictionary<string, string>();

            if (m_Data.ContainsKey(data.Item1))
                m_Data[data.Item1] = data.Item2;
        }

        public void AddData(Tuple<string, string> data)
        {
            if (m_Data == null)
                m_Data = new Dictionary<string, string>();

            if (!m_Data.ContainsKey(data.Item1))
                m_Data.Add(data.Item1, data.Item2);
        }

        public void MasterApplyData(SynchronizableStringData data)
        {
            if (data == null)
                return;

            m_Data = data.Data;
        }
    }
}
