﻿using Photon.Pun;
using UnityEngine;
using Virtuloc.Networking.Shared;


namespace Virtuloc.Networking
{
    [RequireComponent(typeof(PhotonView))]
    public class BaseSynchronizableObject : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private ISynchronizableObject m_Target;

        public ISynchronizableObject Target { get => m_Target; }        
       
    }
}
