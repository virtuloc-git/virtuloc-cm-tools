﻿using System;

namespace Virtuloc.Networking.Shared
{
    public interface ISynchronizableDataObject
    {
        event Action OnSynchronizationRequested;

        event Action<Tuple<string, string>> OnDataChanged;
        event Action<Tuple<string, string>> OnDataAdded;
        event Action<string> OnDataDeleted;

        SynchronizableStringData GetSynchronizedData();

        void ChangeData(Tuple<string, string> data);
        void AddData(Tuple<string, string> data);
        void DeleteData(string key);

        void MasterApplyData(SynchronizableStringData data);
    }
}
