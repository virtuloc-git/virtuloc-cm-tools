﻿using System.Collections.Generic;

namespace Virtuloc.Networking.Shared
{
    public class SynchronizableStringData
    {
        public Dictionary<string, string> Data { get; private set; }

        public SynchronizableStringData()
        {
            Data = new Dictionary<string, string>();
        }

        public SynchronizableStringData(Dictionary<string, string> data)
        {
            Data = data;
        }
    }
}
