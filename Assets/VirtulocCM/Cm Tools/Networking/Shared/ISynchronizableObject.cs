﻿using System;
using System.Collections;
using UnityEngine;

namespace Virtuloc.Networking.Shared
{
    public interface ISynchronizableObject
    {
        event Action OnSynchronizableDataChanged;

        void ApplySynchronizedData(SynchronizableData data);

        SynchronizableData GetSynchronizedData();
    }
}
