﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Virtuloc.Networking.Shared
{
    public class SynchronizableData
    {
        public Dictionary<string, object> Data { get; private set; }

        public SynchronizableData()
        {
            Data = new Dictionary<string, object>();
        }
    }
}
