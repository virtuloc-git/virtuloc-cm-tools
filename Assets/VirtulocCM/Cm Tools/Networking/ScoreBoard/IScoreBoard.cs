﻿namespace Virtuloc.Networking
{
    public interface IScoreBoard
    {
        /// <summary>
        /// Get all entries in score board.
        /// </summary>
        /// <returns></returns>
        string[] GetAll();
        /// <summary>
        /// Get n entries in score board
        /// </summary>
        /// <param name="startPosition">Start position indexed from 0, inclusive</param>
        /// <param name="count">Count of entries</param>
        /// <returns></returns>
        string[] GetMany(int startPosition, int count);
        /// <summary>
        /// Get entry in score board on specific position
        /// </summary>
        /// <param name="position">Position indexed from 0</param>
        /// <returns></returns>
        string GetAtPosition(int position);
        /// <summary>
        /// Get user entry data in score board
        /// </summary>
        /// <returns></returns>
        string GetUserData();
        /// <summary>
        /// Get user position in score board
        /// </summary>
        /// <returns></returns>
        int GetUserPosition();
        /// <summary>
        /// Does user has entry in score board?
        /// </summary>
        /// <returns>True, if user has entry in score board, else False</returns>
        bool UserExists();
        /// <summary>
        /// Set user score into score board
        /// </summary>
        /// <param name="score"></param>
        void SetData(int score);
        /// <summary>
        /// Get count of records in score board.
        /// </summary>
        /// <returns></returns>
        int GetCount();
    }
}