using UnityEngine;
using UnityEngine.Events;

namespace Virtuloc.Networking
{
    public class ScoreBoardBehaviour : MonoBehaviour, IScoreBoard
    {
        [SerializeField] private string _scoreBoardId;

        [Space]
        [Header("Settings")]
        [SerializeField] private bool _loadDataOnStart;
        [SerializeField] private DataScope _scope = DataScope.ROOM;

        [Header("Editor")]
        [SerializeField] private bool _useFakeData;
        [SerializeField] private int _fakeDataCount;

        [Space]
        [SerializeField] private UnityEvent _onScoreBoardUpdated;

        private ScoreBoardController _scoreBoard;

        private IPersistantDataStorage _persistantDataStorage;

        public string[] GetAll()
        {
            return _scoreBoard.GetAll();
        }

        public string GetAtPosition(int position)
        {
            return _scoreBoard.GetAtPosition(position);
        }

        public string[] GetMany(int startPosition, int count)
        {
            return _scoreBoard.GetMany(startPosition, count);
        }

        public string GetUserData()
        {
            return _scoreBoard.GetUserData();
        }

        public int GetUserPosition()
        {
            return _scoreBoard.GetUserPosition();
        }

        public void SetData(int score)
        {
            _scoreBoard.SetData(score);
        }

        public bool UserExists()
        {
            return _scoreBoard.UserExists();
        }

        public int GetCount()
        {
            return _scoreBoard.GetCount();
        }

        private void Awake()
        {
            if (_useFakeData)
                _persistantDataStorage = new TemporaryPersistantDataStorage(_scoreBoardId, _fakeDataCount);
            else
                _persistantDataStorage = new OfflinePersistantDataStorage();

            _scoreBoard = new ScoreBoardController(
                "EditorUniqueId",
                _scoreBoardId,
                "EditorUserName",
                _persistantDataStorage,
                _scope
                );

            _scoreBoard.OnScoreBoardChanged += Handle_scoreBoard_OnScoreBoardChanged;

        }

        private void Handle_scoreBoard_OnScoreBoardChanged()
        {
            _onScoreBoardUpdated?.Invoke();
        }


        private void Start()
        {
            if (_loadDataOnStart)
                _scoreBoard.Refresh();          
        }

        void OnDestroy()
        {
            if (_scoreBoard != null)
            {
                _scoreBoard.OnScoreBoardChanged -= Handle_scoreBoard_OnScoreBoardChanged;
                _scoreBoard.Deinitialize();
            }
        }

    }
}
