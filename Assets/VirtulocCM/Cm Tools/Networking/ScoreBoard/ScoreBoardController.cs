﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Virtuloc.Networking
{
    public class ScoreBoardController : IScoreBoard
    {
        public event Action<string, ScoreBoardEntity> OnScoreBoardEntityChanged;
        public event Action OnScoreBoardChanged;

        private const int ENTITY_PARTS_COUNT = 3;
        public static readonly char ENTITY_SEPARATOR = ';';

        private SortedDictionary<string, ScoreBoardEntity> _scoreBoard;

        private IPersistantDataStorage _persistantDataStorage;

        private DataScope _scope;

        private string _scoreBoardDataKey => $"ScoreBoard.{_scoreBoardId}";

        private string _scoreBoardId;
        private string _userUniqueId;
        private string _username;

        public ScoreBoardController(string userUniqueId, string scoreBoardId, IPersistantDataStorage persistantDataStorage, DataScope scope)
        {
            _userUniqueId = userUniqueId;
            _scoreBoardId = scoreBoardId;
            _persistantDataStorage = persistantDataStorage;

            _scope = scope;

            _persistantDataStorage.OnPersistantDataUpdated += Handle_persistantDataStorage_OnPersistantDataUpdated;
            _persistantDataStorage.OnDataRecieved += Handle_persistantDataStorage_OnDataRecieved;
        }

        public ScoreBoardController(string userUniqueId, string scoreBoardId, string username, IPersistantDataStorage persistantDataStorage, DataScope scope) : this(userUniqueId, scoreBoardId, persistantDataStorage, scope)
        {
            _username = username;
        }

        private void Handle_persistantDataStorage_OnPersistantDataUpdated(object sender, PersistantDataUpdatedEventArgs args)
        {
            if (args.Key.StartsWith(_scoreBoardDataKey))
                _persistantDataStorage.GetData(args.Key, _scope);
        }

        private void Handle_persistantDataStorage_OnDataRecieved(object sender, PersistantDataRecievedEventArgs e)
        {
            InitScoreBoard();

            if (e.Data == null)
                return;

            foreach (var item in e.Data)
            {
                var entity = ParseScoreBoardEntity(item.Value);

                if (entity != null)
                    _scoreBoard[item.Key] = entity;
            }

            OnScoreBoardChanged?.Invoke();
        }

        public void Refresh()
        {
            LoadScoreBoardData();
        }

        private void LoadScoreBoardData()
        {
            _persistantDataStorage.GetData(_scoreBoardDataKey, _scope);           
        }

        private void ClearScoreBoard()
        {
            if (_scoreBoard == null)
                _scoreBoard = new SortedDictionary<string, ScoreBoardEntity>();
            else
                _scoreBoard.Clear();
        }

        private void InitScoreBoard()
        {
            if (_scoreBoard == null)
                _scoreBoard = new SortedDictionary<string, ScoreBoardEntity>();
        }

        private ScoreBoardEntity ParseScoreBoardEntity(string entityString)
        {
            var parts = entityString.Split(ENTITY_SEPARATOR);

            var entity = new ScoreBoardEntity();

            if (parts.Length < ENTITY_PARTS_COUNT)
            {
                Debug.Log($"Failed to parse score board entity. {entity}");
                return null;
            }

            if (int.TryParse(parts.Last(), out int score))
                entity.Score = score;

            entity.UniqueId = parts[0];

            entity.UserName = parts.Length > ENTITY_PARTS_COUNT ? string.Join(ENTITY_SEPARATOR.ToString(), parts.Skip(1).Take(parts.Length - 1)) : parts[1];

            return entity;
        }

        private string[] SerializeOrderedEntities(IEnumerable<ScoreBoardEntity> data, int startIndex, int count)
        {
            var orderedData = OrderEntities(data);

            return SerializeEntities(orderedData.Skip(startIndex).Take(count));
        }

        private string[] SerializeOrderedEntities(IEnumerable<ScoreBoardEntity> data)
        {
            var orderedData = OrderEntities(data);

            return SerializeEntities(orderedData);
        }

        private IEnumerable<ScoreBoardEntity> OrderEntities(IEnumerable<ScoreBoardEntity> data)
        {
            return data.OrderByDescending(x => x.Score).ThenBy(x => x.UserName);
        }

        private static string[] SerializeEntities(IEnumerable<ScoreBoardEntity> data)
        {
            var array = new string[data.Count()];

            for (int i = 0; i < data.Count(); i++)
            {
                array[i] = data.ElementAt(i).SerializeData();
            }

            return array;
        }

        private void UpdatePersistantData(string dataKey, ScoreBoardEntity entity)
        {
            _persistantDataStorage.SetData(dataKey, entity.ToString(), _scope);
        }

        public string[] GetAll()
        {
            if (_scoreBoard == null)
                Refresh();

            return SerializeOrderedEntities(_scoreBoard.Values);
        }

        public string[] GetMany(int startPosition, int count)
        {
            if (_scoreBoard == null)
                Refresh();

            return SerializeOrderedEntities(_scoreBoard.Values, startPosition, count);
        }

        public string GetAtPosition(int position)
        {
            if (_scoreBoard == null)
                Refresh();

            var orderedData = OrderEntities(_scoreBoard.Values);

            if (position < 0 || position >= orderedData.Count())
                return null;

            return orderedData.ElementAt(position)?.SerializeData();
        }

        public string GetUserData()
        {
            if (_scoreBoard == null)
                Refresh();

            var orderedData = OrderEntities(_scoreBoard.Values);

            return orderedData.FirstOrDefault(x => x.UniqueId == _userUniqueId)?.SerializeData() ?? null;
        }

        public int GetUserPosition()
        {
            if (_scoreBoard == null)
                Refresh();

            var orderedData = OrderEntities(_scoreBoard.Values).ToList();

            var record = orderedData.FirstOrDefault(x => x.UniqueId == _userUniqueId);

            return record == null ? -1 : orderedData.IndexOf(record);
        }

        public bool UserExists()
        {
            if (_scoreBoard == null)
                Refresh();

            return _scoreBoard.Any(x => x.Value.UniqueId == _userUniqueId);
        }

        public int GetCount()
        {
            return _scoreBoard.Count;
        }

        public void SetData(int score)
        {
            if (_scoreBoard == null)
                Refresh();

            string key = $"{_userUniqueId}.{_scoreBoard.Count(x => x.Value.UniqueId == _userUniqueId)}";

            var newEntity = new ScoreBoardEntity()
            {
                UserName = _username,
                UniqueId = _userUniqueId,
                Score = score
            };

            UpdatePersistantData($"{_scoreBoardDataKey}.{key}", newEntity);
        }

        public void Deinitialize()
        {
            _persistantDataStorage.OnPersistantDataUpdated -= Handle_persistantDataStorage_OnPersistantDataUpdated;
        }
    }
}
