﻿namespace Virtuloc.Networking
{
    public class ScoreBoardEntity
    {
        public string UniqueId { get; set; }
        public string UserName { get; set; }
        public int Score { get; set; }

        public override string ToString()
        {
            return $"{UniqueId}{ScoreBoardController.ENTITY_SEPARATOR}{UserName}{ScoreBoardController.ENTITY_SEPARATOR}{Score}";
        }

        public string SerializeData()
        {
            return $"{UserName}{ScoreBoardController.ENTITY_SEPARATOR}{Score}";
        }
    }
}
