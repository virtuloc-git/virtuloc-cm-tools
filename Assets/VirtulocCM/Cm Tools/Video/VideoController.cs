using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Video
{
    public class VideoController : MonoBehaviour
    {
        [SerializeField] private VideoPlayer _videoPlayer;
        [SerializeField] private RawImage _rawImage;

        [SerializeField] private Image[] _thumbnailImages;
        [SerializeField] private Button _leftButton;
        [SerializeField] private Button _rightButton;

        private RenderTexture _renderTexture;

        private void CreateRenderTexture()
        {
            _renderTexture = new RenderTexture((int) _videoPlayer.width, (int) _videoPlayer.height, 0);
        }

        public void UI_OnVideoClicked()
        {
        }

        public void UI_OnLeftButtonClicked()
        {
        }
        
        public void UI_OnRightButtonClicked()
        {
        }
        
        public void UI_OnThumbnailClicked(int index)
        {
        }
    }
}