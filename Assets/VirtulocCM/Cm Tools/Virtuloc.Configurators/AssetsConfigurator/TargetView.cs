﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.Configurators
{
    [ExecuteInEditMode]
    public class TargetView : MonoBehaviour
    {
#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            var dimension = Vector3.one;

            var position = transform.position + new Vector3(0f, dimension.y / 2f, 0f);
            Gizmos.color = new Color32(255, 0, 0, 127);
            Gizmos.DrawWireCube(position, dimension);
        }
#endif
    }

}