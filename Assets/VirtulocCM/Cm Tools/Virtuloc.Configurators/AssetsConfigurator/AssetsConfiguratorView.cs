﻿using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Configurators
{
    public class AssetsConfiguratorView : MonoBehaviour
    {
        [SerializeField]
        private AssetsConfigurator _configurator;

        [SerializeField]
        private Canvas _canvas;

        [Header("Buttons")]
        [SerializeField]
        private Button _nextButton;
        [SerializeField]
        private Button _previousButton;

        [Header("Loading")]
        [SerializeField]
        private GameObject _loadingPrefab;
        [SerializeField]
        private Transform _loadingParent;

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            var targetDimension = Vector3.one;
            var loadingRadius = 0.25f;

            var targetPosition = _configurator.Target.position + new Vector3(0f, targetDimension.y /2f, 0f);
            var loadingPosition = _loadingParent.transform.position + new Vector3(0f, loadingRadius / 2f, 0f);

            Gizmos.color = new Color32(255, 0, 0, 127);
            Gizmos.DrawWireCube(targetPosition, targetDimension);
            Gizmos.DrawWireSphere(loadingPosition, loadingRadius);
        }
#endif
    }

}
