﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Virtuloc.CmTools.Shared;

namespace Virtuloc.Configurators
{
    [RequireComponent(typeof(Collider))]
    public class OpenConfiguratorUrlOnClik : MonoBehaviour, IClickableObject
    {
        public Action OnClick;

        private void Start()
        {
            if (gameObject.layer != 10)
                gameObject.layer = 10;
        }

        public void Click()
        {
            OnClick?.Invoke();
        }

        private void OnValidate()
        {
            gameObject.layer = 10;
        }
    }
}
