﻿using Configurator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Virtuloc.Configurators
{
    public class AssetsConfigurator : PointConfigurator<AssetsConfiguratorModel>
    {
        [SerializeField]
        private Transform m_Target;

        public Transform Target => m_Target;

        protected override void ApplyConfiguration()
        {

        }

#if UNITY_EDITOR

        protected override Color32 m_GizmoColor => new Color32(255, 0, 0, 127);
#endif
    }

    public class AssetsConfiguratorModel
    {
        
    }    
}
