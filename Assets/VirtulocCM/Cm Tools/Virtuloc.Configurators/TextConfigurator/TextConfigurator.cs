﻿using Configurator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Configurators
{
    public class TextConfigurator : PointConfigurator<TextConfiguratorModel>
    {
        [SerializeField]
        private TMPro.TMP_Text _text;

        [SerializeField]
        private List<TMPro.TMP_FontAsset> _possibleFonts;

        [SerializeField]
        private Image _backgroundImage;

        protected override void ApplyConfiguration()
        {
        }     
    }

    public class TextConfiguratorModel
    {
        public int FontID { get; set; } = -1;
        public float FontSize { get; set; } = -1f;
        public string Text { get; set; }

        public bool UseBackground;
        public Color32 TextColor { get; set; }
        public Color32 BackgroundColor { get; set; }

        public HorizontalAlignment HorizontalAlignment { get; set; }
        public VerticalAlignment VerticalAlignment { get; set; }

    }

    public enum HorizontalAlignment
    {
        LEFT,
        MIDDLE,
        RIGHT
    }
    public enum VerticalAlignment
    {
        TOP,
        MIDDLE,
        BOTTOM
    }
}
