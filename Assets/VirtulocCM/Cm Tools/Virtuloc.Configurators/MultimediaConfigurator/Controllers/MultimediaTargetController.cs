﻿using UnityEngine;
using UnityEngine.UI;
using Virtuloc.Tweens;

namespace Virtuloc.Configurators
{
    [RequireComponent(typeof(Button))]
    public class MultimediaTargetController : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _infoPanel;

        [SerializeField]
        private Sprite _infoButtonOpenSprite;

        [SerializeField]
        private Sprite _infoButtonCloseSprite;

        [SerializeField]
        private Image _infoButtonIconImage;

        [SerializeField]
        private TMPro.TMP_Text _infoTitle;

        [SerializeField]
        private TMPro.TMP_Text _infoText;

        [SerializeField]
        private TMPro.TMP_Text _mediaTitle;

        [Header("Buttons")]
        [SerializeField]
        private Button _webUrlButton;
        [SerializeField]
        private Button _infoButton;
        
        [Header("Animations")]
        [SerializeField] 
        private TweenPlayer _infoPanelAnimation;
    }
}
