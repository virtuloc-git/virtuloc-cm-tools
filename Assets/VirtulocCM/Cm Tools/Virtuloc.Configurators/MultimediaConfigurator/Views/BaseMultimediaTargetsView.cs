﻿using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Configurators
{
    public abstract class BaseMultimediaTargetsView : MonoBehaviour
    {
        [SerializeField]
        private string _name;

        [SerializeField]
        private Button _previousArrow;

        [SerializeField]
        private Button _nextArrow;

        private void OnValidate()
        {
            if (string.IsNullOrEmpty(_name))
                _name = gameObject.name;
        }
    }
}
