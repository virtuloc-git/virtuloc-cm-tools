﻿using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.Configurators
{
    public class MultimediaTargetsView : BaseMultimediaTargetsView
    {
        [SerializeField]
        private List<MultimediaTargetController> _targets;

        public List<MultimediaTargetController> Targets => _targets;
    }
}
