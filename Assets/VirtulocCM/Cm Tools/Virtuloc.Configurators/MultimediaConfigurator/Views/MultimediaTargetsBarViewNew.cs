﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Configurators
{
    public class MultimediaTargetsBarViewNew : BaseMultimediaTargetsView
    {
        [SerializeField]
        private MultimediaTargetController _target;

        [SerializeField]
        private RectTransform _uiParent;

        [SerializeField]
        private List<Button> _thumbnails;

        //[SerializeField]
        //private RectTransform _infoPanel;

        //[SerializeField]
        //private TMPro.TMP_Text _infoTitle;

        //[SerializeField]
        //private TMPro.TMP_Text _infoText;

        //[SerializeField]
        //private TMPro.TMP_Text _mediaTitle;

        //[Header("Buttons")]
        //[SerializeField]
        //private Button _webUrlButton;
        //[SerializeField]
        //private Button _infoButton;
    }
}
