﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Configurators
{
    [System.Obsolete("Use MultimediaTargetsBarViewNew instead.")]
    public class MultimediaTargetsBarView : BaseMultimediaTargetsView
    {
        [SerializeField]
        private MultimediaTargetController _target;

        [SerializeField]
        private RectTransform _uiParent;

        [SerializeField]
        private List<Button> _thumbnails;
    }
}
