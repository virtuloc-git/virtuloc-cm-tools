﻿using Configurator;
using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Configurators
{
    public class MultimediaConfigurator : PointConfigurator<MultimediaConfiguratorModel>
    {
        [SerializeField]
        private MultimediaSlideshowModel _slideshowSettings;

        public MultimediaSlideshowModel SlideshowSettings => _slideshowSettings;

        protected override void ApplyConfiguration()
        {
            
        }
    }
}