﻿using System.Collections.Generic;

namespace Virtuloc.Configurators
{
    public class MultimediaConfiguratorModel
    {
        public List<MultimediaDto> Medias { get; set; }
    }
}