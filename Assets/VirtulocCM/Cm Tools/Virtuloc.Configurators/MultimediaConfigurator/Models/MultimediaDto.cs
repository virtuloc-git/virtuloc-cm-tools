﻿namespace Virtuloc.Configurators
{
    public class MultimediaDto
    {
        public int Id { get; set; }
        public MultimediaType MultimediaType { get; set; }
        public string Url { get; set; }
    }
}