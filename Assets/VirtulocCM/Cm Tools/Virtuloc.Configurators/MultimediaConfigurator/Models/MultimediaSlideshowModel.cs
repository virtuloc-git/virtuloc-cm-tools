﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.Configurators
{
    [Serializable]
    public class MultimediaSlideshowModel
    {
        [SerializeField]
        private List<BaseMultimediaTargetsView> _possibleViews;

        public List<BaseMultimediaTargetsView> PossibleViews => _possibleViews;

    }

}
