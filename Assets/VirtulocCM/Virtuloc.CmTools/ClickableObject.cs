﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Virtuloc.CmTools.Shared;

namespace Virtuloc.CmTools
{
    [RequireComponent(typeof(Collider))]
    public class ClickableObject : MonoBehaviour, IClickableObject
    {
        public UnityEvent OnClick;

        private void Start()
        {
            if (gameObject.layer != 10)
                gameObject.layer = 10;
        }

        public void Click()
        {
            OnClick?.Invoke();
        }

        private void OnValidate()
        {
            gameObject.layer = 10;
        }

        private void OnMouseUp()
        {
            Click();
        }
    }
}
