using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.CmTools.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class VirtulocAudioSource : MonoBehaviour
    {
        /// <summary>
        /// Type of the audio source
        /// </summary>
        [SerializeField, Tooltip("AudioSource type for audio processing.")] 
        private VirtulocCM_AudioSourceType _audioSourceType;

        private AudioSource _audioSource;
        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();

            var player = GameObject.FindGameObjectWithTag("CM_Player");
        }
    }

    /// <summary>
    /// Type of the audio source
    /// </summary>
    public enum  VirtulocCM_AudioSourceType
    {
        /// <summary>
        /// Default state - Unmutable
        /// </summary>
        NONE = 0,
        /// <summary>
        /// Mutable by MAIN sounds
        /// </summary>
        BACKGROUND_MUSIC = 1,
        /// <summary>
        /// Unmutable
        /// </summary>
        SOUND_EFFECT = 2,
        /// <summary>
        /// Unmutable
        /// </summary>
        ENVIROMENTAL = 3,
        /// <summary>
        /// This source will mute other mutable sources
        /// </summary>
        MAIN_SOUND = 4,
    }
}