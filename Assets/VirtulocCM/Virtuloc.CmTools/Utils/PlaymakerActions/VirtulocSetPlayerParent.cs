using HutongGames.PlayMaker;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Virtuloc.CmTools.Loggers;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Virtuloc.CmTools.Utils
{
    [ActionCategory("Virtuloc")]
    [Tooltip("Set parent object of Virtuloc player.")]
    public class VirtulocSetPlayerParent : FsmStateAction
    {
        [Tooltip("VirtulocCmUtils behaviour.")]
        public VirtulocCmUtils VirtulocCmUtils;

        [Tooltip("Lock player camera while parented.")]
        public FsmBool LockCamera;
        [Tooltip("Lock player movement while parented.")]
        public FsmBool LockMovement = true;
        [Tooltip("Parent object of Virtuloc player.")]
        public FsmGameObject Parent;

        public override void Reset()
        {
            LockCamera = false;
            LockMovement = true;
            Parent = null;
            VirtulocCmUtils = null;
        }

        public override void OnEnter()
        {
            if(VirtulocCmUtils == null)
            {
                var behaviour = GameObject.FindObjectOfType<VirtulocCmUtils>();
                if(behaviour == null)
                {
                    var go = new GameObject("VirtulocCmUtils_Created");
                    behaviour = go.AddComponent<VirtulocCmUtils>();
                }

                VirtulocCmUtils = behaviour;
            }

            VirtulocCmUtils.SetPlayerParent(Parent.Value.transform, LockCamera.Value, LockMovement.Value);

            Finish();
        }
    }
}
