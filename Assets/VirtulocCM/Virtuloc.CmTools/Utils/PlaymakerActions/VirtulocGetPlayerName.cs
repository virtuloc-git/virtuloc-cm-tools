﻿using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Virtuloc.CmTools.Utils
{
    [ActionCategory("Virtuloc")]
    [Tooltip("Get player name.")]
    public class VirtulocGetPlayerName : FsmStateAction
    {
        [Tooltip("VirtulocCmUtils behaviour.")]
        public VirtulocCmUtils VirtulocCmUtils;

        [Tooltip("Out variable for player name.")]
        public FsmString PlayerName;

        public override void Reset()
        {
            VirtulocCmUtils = null;
        }

        public override void OnEnter()
        {
            if (VirtulocCmUtils == null)
            {
                var behaviour = GameObject.FindObjectOfType<VirtulocCmUtils>();
                if (behaviour == null)
                {
                    var go = new GameObject("VirtulocCmUtils_Created");
                    behaviour = go.AddComponent<VirtulocCmUtils>();
                }

                VirtulocCmUtils = behaviour;
            }

            PlayerName.Value = VirtulocCmUtils.GetPlayerName();

            Finish();
        }
    }
}
