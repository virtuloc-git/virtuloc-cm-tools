﻿using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Virtuloc.CmTools.Utils
{
    [ActionCategory("Virtuloc")]
    [Tooltip("Unparent Virtuloc player.")]
    public class VirtulocUnparentPlayer : FsmStateAction
    {
        [Tooltip("VirtulocCmUtils behaviour.")]
        public VirtulocCmUtils VirtulocCmUtils;

        public override void Reset()
        {
            VirtulocCmUtils = null;
        }

        public override void OnEnter()
        {
            if (VirtulocCmUtils == null)
            {
                var behaviour = GameObject.FindObjectOfType<VirtulocCmUtils>();
                if (behaviour == null)
                {
                    var go = new GameObject("VirtulocCmUtils_Created");
                    behaviour = go.AddComponent<VirtulocCmUtils>();
                }

                VirtulocCmUtils = behaviour;
            }

            VirtulocCmUtils.SetPlayerParent(null, false);

            Finish();
        }
    }
}
