﻿using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Virtuloc.CmTools.Utils
{
    [ActionCategory("Virtuloc")]
    [Tooltip("Get state of Virtuloc application.")]
    public class VirtulocGetApplicationState : FsmStateAction
    {
        [Tooltip("VirtulocCmUtils behaviour.")]
        public VirtulocCmUtils VirtulocCmUtils;

        [Tooltip("State of Virtuloc application (VirtulocCm_ApplicationState).")]
        public FsmEnum ApplicationState;

        public override void Reset()
        {
            VirtulocCmUtils = null;
        }

        public override void OnEnter()
        {
            if (VirtulocCmUtils == null)
            {
                var behaviour = GameObject.FindObjectOfType<VirtulocCmUtils>();
                if (behaviour == null)
                {
                    var go = new GameObject("VirtulocCmUtils_Created");
                    behaviour = go.AddComponent<VirtulocCmUtils>();
                }

                VirtulocCmUtils = behaviour;
            }

            ApplicationState.Value = VirtulocCmUtils.GetApplicationType();

            Finish();
        }
    }
}
