﻿using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Virtuloc.CmTools.Utils
{
    [ActionCategory("Virtuloc")]
    [Tooltip("Get parent of Virtuloc player.")]
    public class VirtulocGetPlayerParent : FsmStateAction
    {
        [Tooltip("VirtulocCmUtils behaviour.")]
        public VirtulocCmUtils VirtulocCmUtils;

        [Tooltip("Parent of Virtuloc player.")]
        public FsmGameObject OutPlayerParent;

        public override void Reset()
        {
            VirtulocCmUtils = null;
        }

        public override void OnEnter()
        {
            if (VirtulocCmUtils == null)
            {
                var behaviour = GameObject.FindObjectOfType<VirtulocCmUtils>();
                if (behaviour == null)
                {
                    var go = new GameObject("VirtulocCmUtils_Created");
                    behaviour = go.AddComponent<VirtulocCmUtils>();
                }

                VirtulocCmUtils = behaviour;
            }

            OutPlayerParent.Value = VirtulocCmUtils.GetPlayerParent()?.gameObject;

            Finish();
        }
    }
}
