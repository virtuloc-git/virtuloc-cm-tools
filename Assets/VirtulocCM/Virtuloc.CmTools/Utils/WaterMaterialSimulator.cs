﻿using UnityEngine;

namespace Virtuloc.CmTools.Utils
{
    public class WaterMaterialSimulator : MonoBehaviour
    {
        /// <summary>
        /// Materiál který chci nastavit
        /// </summary>
        [SerializeField]
        private Material m_Material;

        /// <summary>
        /// Rychlost a směr pohybu hlavní mapy. Rychlost je vstažená k pozici v UV mapě za sekundu.
        /// </summary>
        [SerializeField]
        private Vector2 m_SpeedMainMap;

        /// <summary>
        /// Rychlost a směr pohybu sekundární mapy. Rychlost je vstažená k pozici v UV mapě za sekundu.
        /// </summary>
        [SerializeField]
        private Vector2 m_SpeedSecondaryMap;

        private Vector2 m_MainOffset;
        private Vector2 m_SecondaryOffset;

        /// <summary>
        /// V updatu přepočítá rychlost na na aktuální délku framu a nastaví materiál.
        /// </summary>
        private void FixedUpdate()
        {
            m_MainOffset += (Time.fixedDeltaTime * m_SpeedMainMap);

            m_SecondaryOffset += (Time.fixedDeltaTime * m_SpeedSecondaryMap);

            m_Material.SetTextureOffset("_MainTex", m_MainOffset);
            m_Material.SetTextureOffset("_DetailAlbedoMap", m_SecondaryOffset);
        }
    }
}
