using UnityEngine;

namespace Virtuloc.CmTools.Utils
{
    public class VirtulocCmUtils : MonoBehaviour
    {
        [SerializeField]
        private VirtulocCm_ApplicationState _cmApplicationState = VirtulocCm_ApplicationState.EVENT;

        public int GetPlayerDarkRiftID() => 0;
        public string GetPlayerUUID() => "PlayerUniqueIdentifier";

        public GameObject GetLocalPlayerGameObject() 
        {
            if(Camera.main != null)
                return Camera.main.gameObject;
            else
                return null;
        }

        #region [States]
        public VirtulocCm_ApplicationState GetApplicationType()
        {
#if UNITY_EDITOR
            return _cmApplicationState;
#else
            return VirtulocCm_ApplicationState.EVENT;
#endif
        }

        public VirtulocCm_NetworkingState GetNetworkingState() => VirtulocCm_NetworkingState.IN_ROOM;
#endregion
        
        #region [Player Teleport]
        public void TeleportPlayer(Vector3 position) => Debug.Log($"Teleport player to position: {position}");

        public void TeleportPlayer(Vector3 position, Vector3 rotation) => Debug.Log($"Teleport player to position: {position} rotation: {rotation}");

        #endregion

        #region [PlayerUtils]
        public void SetPlayerParent(Transform parent, bool lockCamera, bool lockMovement = true)
        {
            var player = GameObject.FindGameObjectWithTag("CM_Player");
            Debug.Log($"Set player parent: {parent} lockCamera: {lockCamera} lockMovement: {lockMovement}");

            if (player != null)
            {
                player.transform.SetParent(parent);

                if (parent != null)
                {
                    player.transform.localPosition = Vector3.zero;
                    player.transform.localRotation = Quaternion.identity;
                }
            }
        }

        public Transform GetPlayerParent()
        {
            var player = GameObject.FindGameObjectWithTag("CM_Player");
            Debug.Log($"Get player parent : {player.transform.parent}");

            if (player != null)
            {
                return player.transform.parent;
            }
            else
            {
                return null;
            }
        }

        public string GetPlayerName() => "PlayerName";

        #endregion

        #region [Player Camera]
        public void SetPlayerCameraMode(VirtulocCm_PlayerCameraMode cameraMode) => Debug.Log($"Set player camera mode: {cameraMode.ToString()}");
        public VirtulocCm_PlayerCameraMode GetPlayerCameraMode() => VirtulocCm_PlayerCameraMode.THIRD_PERSON;
        public void ForcePlayerFirstPersonCameraMode() => Debug.Log("Force Player FPS Camera Mode");
        public void RestorePlayerCameraMode() => Debug.Log("Restore Player Camera Mode");
        
        #endregion
    }

    #region [Enums]
    public enum VirtulocCm_PlayerCameraMode{
        FIRST_PERSON = 0,
        THIRD_PERSON = 1
    }

    public enum VirtulocCm_NetworkingState
    {
        DISCONNECTED = 0,
        CONNECTING = 1,
        IN_LOBBY = 2,
        IN_ROOM = 3
    }

    public enum VirtulocCm_ApplicationState
    {
        INIT = 0,
        CONFIGURATOR = 1,
        PLAYER = 2,
        EVENT = 3
    }
    #endregion
}
