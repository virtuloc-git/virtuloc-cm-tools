using UnityEngine;

public class WorldDimensions : MonoBehaviour, IWorldDimensions
{
    [SerializeField] private Vector2 _xDimensions;
    [SerializeField] private Vector2 _yDimensions;
    [SerializeField] private Vector2 _zDimensions;

    public Vector2 X => _xDimensions;
    public Vector2 Y => _yDimensions;
    public Vector2 Z => _zDimensions;

}