using UnityEngine;

public interface IWorldDimensions
{
    Vector2 X { get; }
    Vector2 Y { get; }
    Vector2 Z { get; }
}