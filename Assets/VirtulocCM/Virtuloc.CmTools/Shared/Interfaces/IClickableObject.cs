﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.CmTools.Shared
{
    public interface IClickableObject 
    {
        void Click();
    }
}
