using Configurator;
using UnityEngine;

namespace Virtuloc.CmTools
{
    public class ConfiguratorIdGetter : MonoBehaviour
    {
        public string GetParentConfiguratorId()
        {
            var idProvider = GetComponent<IConfiguratorIdProvider>();

            if (idProvider != null)
                idProvider = transform.GetComponentByInterfaceInParentRecursively<IConfiguratorIdProvider>();

            if(idProvider == null)
                idProvider = transform.GetHighestParent().GetComponentByInterfaceInChildrenRecursively<IConfiguratorIdProvider>();

            if (idProvider == null)
                return string.Empty;

            return idProvider.ConfiguratorId;
        }
    }
}
