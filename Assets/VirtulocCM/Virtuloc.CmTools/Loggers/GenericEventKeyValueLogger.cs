﻿namespace Virtuloc.CmTools.Loggers
{
   public class GenericEventKeyValueLogger : GenericEventLogger
    {
        public string Key;
        public string Value;

        public virtual void LogEvent()
        {
            LogGenericEvent(Key, Value, null);
        }
    }
}
