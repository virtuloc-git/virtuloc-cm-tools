﻿using HutongGames.PlayMaker;
using UnityEngine;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

namespace Virtuloc.CmTools.Loggers
{
    [ActionCategory("Virtuloc")]
    [Tooltip("Log generic event to virtuloc event logger.")]
    public class VirtulocGenericEventLogger : FsmStateAction
    {
        [Tooltip("Key for generic event.")]
        public FsmString Key;
        [Tooltip("Value for generic event.")]
        public FsmString Value;
        [Tooltip("Duration of the generic event.")]
        public FsmFloat Duration;

        public override void Reset()
        {
            Key = null;
            Value = null;
            Duration = null;
        }

        public override void OnEnter()
        {
            var durationString = Duration.Value == 0 ? "" : $", DURATION: {Duration.Value}";
            Debug.Log($"[{nameof(VirtulocGenericEventLogger)}]: Logging generic event: KEY: {Key}, VALUE: {Value}{durationString}");
            Finish();
        }
    }
}
