﻿namespace Virtuloc.CmTools.Loggers
{
    public class GenericEventKeyValueDurationLogger : GenericEventKeyValueLogger
    {
        public float Duration;

        public override void LogEvent()
        {
            LogGenericEvent(Key, Value, Duration);
        }
    }
}
