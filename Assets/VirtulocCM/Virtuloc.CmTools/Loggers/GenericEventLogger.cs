using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.CmTools.Loggers
{
    public class GenericEventLogger : MonoBehaviour
    {
        public void LogGenericEvent(string key, string value, float? duration)
        {
            var durationString = duration.HasValue ? "" : $", DURATION: {duration.Value}";
            Debug.Log($"[{nameof(GenericEventLogger)}]: Logging generic event: KEY: {key}, VALUE: {value}{durationString}");
        }
    }
}
