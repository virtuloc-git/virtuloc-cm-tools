﻿//using NativeApi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtuloc.CmTools
{
    public class OpenUrl : MonoBehaviour
    {
        [SerializeField]
        private string _url;

        [SerializeField]
        private bool _iFrame;

        [SerializeField]
        [Range(0f, 100f)]
        private int _iFrameWidth = 100;

        [SerializeField]
        [Range(0f, 100f)]
        private int _iframeHeight = 100;

        public string Url => _url;
        public bool IFrame => _iFrame;
        public float IFrameWidth => _iFrameWidth;
        public float IframeHeight => _iframeHeight;

        public void Open()
        {
            if (string.IsNullOrEmpty(_url))
            {
                Debug.LogError("Can not open empty url.", gameObject);
                return;
            }

            if (!_url.StartsWith("http://") && !_url.StartsWith("https://"))
                _url = $"https://{_url}";

            //NativeApiConnector.Instance.OpenUrl(_url, _iFrame ? OpenType.POPUP : OpenType.TAB, _iFrameWidth.ToString());

            Debug.Log($"[OpenUrl]: Open url: {_url}  width: {_iFrameWidth}, height: {_iframeHeight} , object name: {gameObject.name}", gameObject);
        }
    }
}
