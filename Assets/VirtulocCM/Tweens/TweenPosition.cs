using UnityEngine;

namespace Virtuloc.Tweens
{
    public class TweenPosition : TweenWithTargetAndValues<Transform, Vector3>
    {
        [SerializeField] private bool _useWorldPosition;

        public bool UseWorldPosition
        {
            get => _useWorldPosition;
            set => _useWorldPosition = value;
        }

        protected override void Lerp(float t)
        {
            _target.localPosition = Vector3.LerpUnclamped(_from, _to, t);
        }

        protected override void SetDefaultTarget()
        {
            _target = transform;
        }

        protected override void SetDefaultValues()
        {
            _from = transform.localPosition;
            _to = transform.localPosition;
        }
    }
}