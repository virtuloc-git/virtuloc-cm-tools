using UnityEngine;

namespace Virtuloc.Tweens
{
    public abstract class TweenWithTarget<TTarget> : Tween
    {
        [SerializeField] protected TTarget _target;

        public TTarget Target
        {
            get => _target;
            set => _target = value;
        }

        protected abstract void SetDefaultTarget();

#if UNITY_EDITOR
        protected override void Reset()
        {
            base.Reset();
            SetDefaultTarget();
        }
#endif
    }
}