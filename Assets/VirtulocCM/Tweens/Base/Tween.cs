using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Virtuloc.Tweens
{
#if UNITY_EDITOR
    [ExecuteAlways]
#endif
    public abstract class Tween : MonoBehaviour
    {
        [SerializeField, HideInInspector] public AnimationCurve Curve = AnimationCurve.Linear(0, 0, 1, 1);
        [SerializeField, HideInInspector] public float Duration = 1;
        [SerializeField, HideInInspector] public int LoopCount = 1;
        [SerializeField, HideInInspector] public float StartDelay = 0;

        [SerializeField, HideInInspector] public bool PlayOnStart = false;
        [SerializeField, HideInInspector] public bool PlayOnEnabled = false;

        [SerializeField, HideInInspector] public UnityEvent OnStart;
        [SerializeField, HideInInspector] public UnityEvent OnEnd;

#if UNITY_EDITOR
        //Don't want this in builds
        private readonly TweenEditorRunner _editorRunner = new TweenEditorRunner();
#endif

        private bool _running;
        private float _elapsedTime;
        private bool _reverse;

        public bool Running => _running;

        private void Update()
        {
            if (_running)
            {
#if UNITY_EDITOR
                // If not in play mode, use editor delta time
                _elapsedTime += EditorApplication.isPlaying ? Time.deltaTime : _editorRunner.DeltaTime;
#else
                // In build use normal delta time
                _elapsedTime += Time.deltaTime;
#endif

                float loopIndex = Mathf.Max(_elapsedTime - StartDelay, 0) / Duration;

                // Check if should stop
                if (loopIndex >= LoopCount && LoopCount > 0)
                {
#if UNITY_EDITOR
                    // Invoke events only in play mode
                    Stop(EditorApplication.isPlaying);
#else
                    // In build invoke events everytime
                    Stop(true);
#endif
                    return;
                }

                float f = loopIndex - Mathf.Floor(loopIndex);
                float t = Curve.Evaluate(_reverse ? 1 - f : f);
                Lerp(t);
            }
        }

        protected abstract void Lerp(float t);

        private void Start()
        {
#if UNITY_EDITOR
            // Ignore PlayOnStart in edit mode
            if (!EditorApplication.isPlaying)
                return;
#endif

            if (PlayOnStart)
                Play();
        }

        private void OnEnable()
        {
#if UNITY_EDITOR
            // Ignore PlayOnEnabled in edit mode
            // Send message to editor runner
            if (!EditorApplication.isPlaying)
            {
                _editorRunner.OnEnable(this);
                return;
            }
#endif

            if (PlayOnEnabled)
                Play();
        }

        private void OnDisable()
        {
#if UNITY_EDITOR
            // Send message to editor runner
            if (!EditorApplication.isPlaying)
            {
                _editorRunner.OnDisable();
            }
#endif
        }

        public void Play(bool reverse = false)
        {
            _running = true;
            _elapsedTime = 0;
            _reverse = reverse;

            if (_reverse)
                ResetToEnd(true);
            else
                ResetToStart(true);
        }

        public void Stop(bool invokeEvents = true)
        {
            if (_reverse)
                ResetToStart(invokeEvents);
            else
                ResetToEnd(invokeEvents);

            _running = false;
            _reverse = false;
        }

        public void Pause()
        {
            _running = false;
        }

        public void Continue()
        {
            _running = true;
        }

        public void ResetToStart(bool invokeEvents = false)
        {
            Lerp(Curve.Evaluate(0));

            if (invokeEvents)
                OnStart?.Invoke();
        }

        public void ResetToEnd(bool invokeEvents = false)
        {
            Lerp(Curve.Evaluate(1));

            if (invokeEvents)
                OnEnd?.Invoke();
        }

#if UNITY_EDITOR
        protected virtual void Reset()
        {
        }

        #region [Editor runner]

        private class TweenEditorRunner
        {
            public float DeltaTime => _validDeltaTime ? (float) (EditorApplication.timeSinceStartup - _lastTime) : 0;

            private Tween _tween;
            private double _lastTime;

            private bool _validDeltaTime = false;

            public void OnEnable(Tween tween)
            {
                _tween = tween;

                if (!EditorApplication.isPlaying)
                {
                    _lastTime = EditorApplication.timeSinceStartup;
                    EditorApplication.update += Update;
                }
            }

            public void OnDisable()
            {
                // ReSharper disable once DelegateSubtraction
                EditorApplication.update -= Update;
            }

            private void Update()
            {
                _validDeltaTime = true;
                _tween.Update();
                _validDeltaTime = false;
                _lastTime = EditorApplication.timeSinceStartup;
            }
        }

        #endregion

#endif
    }
}