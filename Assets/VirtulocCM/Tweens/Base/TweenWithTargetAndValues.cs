using UnityEngine;

namespace Virtuloc.Tweens
{
    public abstract class TweenWithTargetAndValues<TTarget, TValue> : TweenWithTarget<TTarget>
    {
        [SerializeField] protected TValue _from;
        [SerializeField] protected TValue _to;

        public TValue From
        {
            get => _from;
            set => _from = value;
        }

        public TValue To
        {
            get => _to;
            set => _to = value;
        }

        protected abstract void SetDefaultValues();

#if UNITY_EDITOR
        protected override void Reset()
        {
            base.Reset();
            SetDefaultValues();
        }
#endif
    }
}