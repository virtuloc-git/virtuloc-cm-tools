using UnityEngine;

namespace Virtuloc.Tweens
{
    public class ScaleTween : TweenWithTargetAndValues<Transform, Vector3>
    {
        protected override void Lerp(float t)
        {
            _target.localScale = Vector3.LerpUnclamped(_from, _to, t);
        }

        protected override void SetDefaultTarget()
        {
            _target = transform;
        }

        protected override void SetDefaultValues()
        {
            _from = Vector3.one;
            _to = Vector3.one;
        }
    }
}