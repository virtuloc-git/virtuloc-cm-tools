using UnityEngine;

namespace Virtuloc.Tweens
{
    public class RectTransformTween : TweenWithTargetAndValues<RectTransform, RectTransform>
    {
        [SerializeField] private bool _usePosition = false;
        [SerializeField] private bool _useRotation = false;
        [SerializeField] private bool _useScale = false;

        [Space]
        [SerializeField] private bool _useAnchoredPosition = false;

        [SerializeField] private bool _useAnchors = false;
        [SerializeField] private bool _useSizeDelta = false;
        [SerializeField] private bool _usePivot = false;

        public bool UsePosition
        {
            get => _usePosition;
            set => _usePosition = value;
        }

        public bool UseRotation
        {
            get => _useRotation;
            set => _useRotation = value;
        }

        public bool UseScale
        {
            get => _useScale;
            set => _useScale = value;
        }

        public bool UseAnchoredPosition
        {
            get => _useAnchoredPosition;
            set => _useAnchoredPosition = value;
        }

        public bool UseAnchors
        {
            get => _useAnchors;
            set => _useAnchors = value;
        }

        public bool UseSizeDelta
        {
            get => _useSizeDelta;
            set => _useSizeDelta = value;
        }

        public bool UsePivot
        {
            get => _usePivot;
            set => _usePivot = value;
        }

        protected override void Lerp(float t)
        {
            if (_usePosition)
                _target.position = Vector3.LerpUnclamped(_from.position, _to.position, t);

            if (_useRotation)
                _target.rotation = Quaternion.LerpUnclamped(_from.rotation, _to.rotation, t);

            if (_useScale)
                _target.localScale = Vector3.LerpUnclamped(_from.localScale, _to.localScale, t);

            if (_useAnchoredPosition)
                _target.anchoredPosition = Vector2.LerpUnclamped(_from.anchoredPosition, _to.anchoredPosition, t);

            if (_useAnchors)
            {
                _target.anchorMin = Vector2.LerpUnclamped(_from.anchorMin, _to.anchorMin, t);
                _target.anchorMax = Vector2.LerpUnclamped(_from.anchorMax, _to.anchorMax, t);
            }

            if (_useSizeDelta)
                _target.sizeDelta = Vector2.LerpUnclamped(_from.sizeDelta, _to.sizeDelta, t);

            if (_usePivot)
                _target.pivot = Vector2.LerpUnclamped(_from.pivot, _to.pivot, t);
        }

        protected override void SetDefaultTarget()
        {
            _target = (RectTransform) transform;
        }

        protected override void SetDefaultValues()
        {
        }
    }
}