using UnityEngine;

namespace Virtuloc.Tweens
{
    public class CanvasGroupTween : TweenWithTargetAndValues<CanvasGroup, float>
    {
        protected override void Lerp(float t)
        {
            _target.alpha = Mathf.LerpUnclamped(_from, _to, t);
        }

        protected override void SetDefaultTarget()
        {
            _target = GetComponent<CanvasGroup>();
        }

        protected override void SetDefaultValues()
        {
            _from = 0f;
            _to = 1f;
        }
    }
}