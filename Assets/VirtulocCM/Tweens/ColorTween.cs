using System;
using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Tweens
{
    public class ColorTween : TweenWithTargetAndValues<Graphic, Color>
    {
        protected override void Lerp(float t)
        {
            Target.color = Color.LerpUnclamped(_from, _to, t);
        }

        protected override void SetDefaultTarget()
        {
            _target = GetComponent<Graphic>();
        }

        protected override void SetDefaultValues()
        {
            _from = Color.white;
            _to = Color.white;
        }
    }
}