using UnityEngine;

namespace Virtuloc.Tweens
{
    public class RotationTween : TweenWithTargetAndValues<Transform, Vector3>
    {
        [SerializeField] private bool _useQuaternionLerp = false;

        public bool UseQuaternionLerp
        {
            get => _useQuaternionLerp;
            set => _useQuaternionLerp = value;
        }

        protected override void Lerp(float t)
        {
            if (_useQuaternionLerp)
                _target.localRotation = Quaternion.LerpUnclamped(Quaternion.Euler(_from), Quaternion.Euler(_to), t);
            else
                _target.localRotation = Quaternion.Euler(Vector3.LerpUnclamped(_from, _to, t));
        }

        protected override void SetDefaultTarget()
        {
            _target = transform;
        }

        protected override void SetDefaultValues()
        {
            _from = Vector3.zero;
            _to = Vector3.zero;
        }
    }
}