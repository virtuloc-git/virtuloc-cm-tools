using UnityEngine;

namespace Virtuloc.Tweens
{
    public class TransformTween : TweenWithTargetAndValues<Transform, Transform>
    {
        [SerializeField] private bool _usePosition = false;
        [SerializeField] private bool _useRotation = false;
        [SerializeField] private bool _useScale = false;

        public bool UsePosition
        {
            get => _usePosition;
            set => _usePosition = value;
        }

        public bool UseRotation
        {
            get => _useRotation;
            set => _useRotation = value;
        }

        public bool UseScale
        {
            get => _useScale;
            set => _useScale = value;
        }

        protected override void Lerp(float t)
        {
            if (_usePosition)
                _target.position = Vector3.LerpUnclamped(_from.position, _to.position, t);
            
            if (_useRotation)
                _target.rotation = Quaternion.LerpUnclamped(_from.rotation, _to.rotation, t);
            
            if (_useScale)
                _target.localScale = Vector3.LerpUnclamped(_from.localScale, _to.localScale, t);
            
        }

        protected override void SetDefaultTarget()
        {
            _target = transform;
        }

        protected override void SetDefaultValues()
        {
        }
    }
}