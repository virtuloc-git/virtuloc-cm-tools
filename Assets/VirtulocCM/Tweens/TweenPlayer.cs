using System;
using UnityEngine;

namespace Virtuloc.Tweens
{
    public class TweenPlayer : MonoBehaviour
    {
        [SerializeField] private Tween[] _tweens;

        [SerializeField] private bool _runOnDisabled = false;
        
        public Tween[] Tweens
        {
            get => _tweens;
            set => _tweens = value;
        }

        public void Play(bool reverse = false)
        {
            CallOnAll(tween => tween.Play(reverse));
        }

        public void Stop(bool invokeEvents = true)
        {
            CallOnAll(tween => tween.Stop(invokeEvents));
        }

        public void Pause()
        {
            CallOnAll(tween => tween.Pause());
        }

        public void Continue()
        {
            CallOnAll(tween => tween.Continue());
        }

        public void ResetToStart(bool invokeEvents = false)
        {
            CallOnAll(tween => tween.ResetToStart(invokeEvents));
        }

        public void ResetToEnd(bool invokeEvents = false)
        {
            CallOnAll(tween => tween.ResetToEnd(invokeEvents));
        }

        public void CallOnAll(Action<Tween> action)
        {
            if (action == null || _tweens == null)
                return;

            for (int i = 0; i < _tweens.Length; i++)
            {
                if(_runOnDisabled || _tweens[i].gameObject.activeInHierarchy) 
                    action.Invoke(_tweens[i]);
            }
        }
    }
}