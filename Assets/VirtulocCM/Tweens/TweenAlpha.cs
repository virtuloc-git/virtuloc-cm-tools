using UnityEngine;
using UnityEngine.UI;

namespace Virtuloc.Tweens
{
    public class TweenAlpha : TweenWithTargetAndValues<Graphic, float>
    {
        protected override void Lerp(float t)
        {
            var tmpColor = _target.color;
            tmpColor.a = Mathf.LerpUnclamped(_from, _to, t);
            _target.color = tmpColor;
        }

        protected override void SetDefaultTarget()
        {
            _target = GetComponent<Graphic>();
        }

        protected override void SetDefaultValues()
        {
            _from = 1;
            _to = 1;
        }
    }
}