﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Virtuloc.CM
{
    [InitializeOnLoad]
    public static class AutoInstaller
    {
        private const string VIRTULOC_KEY = "com.virtuloc.cm-tools";
        private const string VIRTULOC_GIT_URL = "https://bitbucket.org/virtuloc-git/virtuloc-cm-tools.git?path=/Assets/VirtulocCM";

        static AutoInstaller()
        {
#if !NOT_INSTALL_VIRTULOC
            Install();
#endif
        }

        private static string ManifestPath
        {
            get
            {
                var projectPath = Directory.GetParent(Application.dataPath).FullName;
                var manifestPath = Path.Combine(projectPath, "Packages", "manifest.json");
                return manifestPath;
            }
        }

        private static void Install()
        {
            var manifest = File.ReadAllText(ManifestPath);

            if (!manifest.Contains(VIRTULOC_KEY))
            {
                InstallVirtulocPackage();
            }
        }

        private static void InstallVirtulocPackage()
        {
            Debug.Log("Instaling Virtuloc package.");
            var manifestLines = File.ReadAllText(ManifestPath).Split('\n').ToList();

            for (int i = 0; i < manifestLines.Count - 1; i++)
            {
                if (!manifestLines[i].Contains("{"))
                {
                    manifestLines.Insert(i, $"\"{VIRTULOC_KEY}\": \"{VIRTULOC_GIT_URL}\",");
                    break;
                }
            }

            var sb = new StringBuilder();

            foreach (var item in manifestLines)
            {
                sb.Append(item).Append('\n');
            }

            File.WriteAllText(ManifestPath, sb.ToString());

            AssetDatabase.Refresh();
        }
    }
}
